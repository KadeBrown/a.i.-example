#include "Matrix4.h"
#include <memory>

Matrix4::Matrix4()
{
	memset(m, 0, sizeof(float) * 16);
	m[0][0] = 1; m[1][1] = 1; m[2][2] = 1; m[3][3] = 1;
}


Matrix4::~Matrix4()
{
}


Vector4 Matrix4::RowToVec(const short r) const 
{
	return Vector4(m[r][0], m[r][1], m[r][2], m[r][3]);
}


Vector4 Matrix4::ColToVec(const short c) const
{
	return Vector4(m[0][c], m[1][c], m[2][c], m[3][c]);
}

Matrix4 Matrix4::Transpose()
{
	Matrix4 r;
	Vector4 col0 = ColToVec(0);
	Vector4 col1 = ColToVec(1);
	Vector4 col2 = ColToVec(2);
	Vector4 col3 = ColToVec(3);
	r.m[0][0] = col0.x;	r.m[1][0] = col1.x;	r.m[2][0] = col2.x;	r.m[3][0] = col3.x;
	r.m[0][1] = col0.y;	r.m[1][1] = col1.y;	r.m[2][1] = col2.y;	r.m[3][1] = col3.y;
	r.m[0][2] = col0.z;	r.m[1][2] = col1.z;	r.m[2][2] = col2.z;	r.m[3][2] = col3.z;
	r.m[0][3] = col0.w;	r.m[1][3] = col1.w;	r.m[2][3] = col2.w;	r.m[3][3] = col3.w;
	return r;
}

Matrix4 Matrix4::operator * (const Matrix4& rhs) const
{
	Matrix4 mat;
	mat.m[0][0] = RowToVec(0).Dot(rhs.ColToVec(0));
	mat.m[0][1] = RowToVec(0).Dot(rhs.ColToVec(1));
	mat.m[0][2] = RowToVec(0).Dot(rhs.ColToVec(2));
	mat.m[0][3] = RowToVec(0).Dot(rhs.ColToVec(3));

	mat.m[1][0] = RowToVec(1).Dot(rhs.ColToVec(0));
	mat.m[1][1] = RowToVec(1).Dot(rhs.ColToVec(1));
	mat.m[1][2] = RowToVec(1).Dot(rhs.ColToVec(2));
	mat.m[1][3] = RowToVec(1).Dot(rhs.ColToVec(3));

	mat.m[2][0] = RowToVec(2).Dot(rhs.ColToVec(0));
	mat.m[2][1] = RowToVec(2).Dot(rhs.ColToVec(1));
	mat.m[2][2] = RowToVec(2).Dot(rhs.ColToVec(2));
	mat.m[2][3] = RowToVec(2).Dot(rhs.ColToVec(3));

	mat.m[3][0] = RowToVec(3).Dot(rhs.ColToVec(0));
	mat.m[3][1] = RowToVec(3).Dot(rhs.ColToVec(1));
	mat.m[3][2] = RowToVec(3).Dot(rhs.ColToVec(2));
	mat.m[3][3] = RowToVec(3).Dot(rhs.ColToVec(3));

	return mat;
}

Matrix4& Matrix4::operator *= (const Matrix4& rhs)
{
	*this = *this * rhs;
	return *this;
}

Matrix4 Matrix4::CreateIdentity()
{
	Matrix4 mat;
	memset(mat.m, 0, sizeof(float)* 16);
	mat.m[0][0] = 1; mat.m[1][1] = 1; mat.m[2][2] = 1; mat.m[3][3] = 1;
	return mat;
}
Matrix4 Matrix4::CreateRotationZ(float r)
{
	Matrix4 mat;
	mat.m[0][0] = cosf(r); mat.m[0][1] = -sinf(r);
	mat.m[1][0] = sinf(r); mat.m[1][1] = cosf(r);

	return mat;
}

Matrix4 Matrix4::CreateRotationX(float r)
{
	Matrix4 mat;
	mat.m[1][1] = cosf(r); mat.m[1][2] = -sinf(r);
	mat.m[2][1] = sinf(r); mat.m[2][2] = cosf(r);

	return mat;
}

Matrix4 Matrix4::CreateRotationY(float r)
{
	Matrix4 mat;
	mat.m[0][0] = cosf(r); mat.m[0][2] = sinf(r);
	mat.m[2][0] = -sinf(r); mat.m[2][2] = cosf(r);

	return mat;
}
Matrix4 Matrix4::CreateScale(float s)
{
	Matrix4 mat;
	mat.m[0][0] = s;
	mat.m[1][1] = s;
	mat.m[2][2] = s;
	return mat;
}

Matrix4 Matrix4::CreateTranslation(float x, float y, float z)
{
	Matrix4 mat;
	mat.m[3][0] = x;
	mat.m[3][1] = y;
	mat.m[3][2] = z;
	return mat;
}

Vector3 operator * (const Vector3& v, const Matrix4& mat)
{
	Vector3 vec;
	vec.x = ((v.x * mat.m[0][0]) + (v.y * mat.m[1][0]) + (v.z * mat.m[2][0]) + mat.m[3][0]);
	vec.y = ((v.x * mat.m[0][1]) + (v.y * mat.m[1][1]) + (v.z * mat.m[2][1]) + mat.m[3][1]);
	vec.z = ((v.x * mat.m[0][2]) + (v.y * mat.m[1][2]) + (v.z * mat.m[2][2]) + mat.m[3][2]);
	return vec;
}

const float* Matrix4::GetPtr()
{
	return *m;
}