#include "Vector2.h"
#include <math.h>

Vector2::Vector2()
{
	x = 0;
	y = 0;
}

Vector2::Vector2(float x, float y)
{
	this->x = x;
	this->y = y;
}

float Vector2::Dot(const Vector2 rhs) const
{
	return ((this->x * rhs.x) + (this->y * rhs.y));
}

float Vector2::Magnitude()
{
	return sqrtf((x * x) + (y * y));
}

Vector2& Vector2::Normalise()
{
	float mag = Magnitude();
	if (mag == 0) return *this;
	x = x / mag;
	y = y / mag;

	return *this;
}

Vector2 Vector2::Normalised()
{
	Vector2 vec;
	float mag = Magnitude();
	if (mag == 0) return vec;
	vec.x = x / mag;
	vec.y = y / mag;
	return vec;
}

float Vector2::Dot(const Vector2& lhs, const Vector2& rhs)
{
	return ((lhs.x * rhs.x) + (lhs.y * rhs.y));
}

Vector2& Vector2::operator = (const Vector2 rhs)
{
	this->x = rhs.x;
	this->y = rhs.y;
	return *this;
}

bool Vector2::operator == (const Vector2 rhs) const
{
	return (x == rhs.x && y == rhs.y) ? true : false;
}

Vector2 Vector2::operator + (const Vector2 rhs) const
{
	return Vector2(this->x + rhs.x, this->y + rhs.y);
}

Vector2& Vector2::operator += (const Vector2 rhs)
{
	this->x += rhs.x;
	this->y += rhs.y;

	return *this;
}

Vector2 Vector2::operator - (const Vector2 rhs) const
{
	return Vector2(this->x - rhs.x, this->y - rhs.y);
}

Vector2& Vector2::operator -= (const Vector2 rhs)
{
	this->x -= rhs.x;
	this->y -= rhs.y;

	return *this;
}

Vector2& Vector2::operator *= (const float rhs)
{
	this->x *= rhs;
	this->y *= rhs;

	return *this;
}

Vector2 Vector2::operator * (const float rhs) const
{
	return Vector2(this->x * rhs, this->y * rhs);
}

Vector2 Vector2::operator / (const float rhs) const
{
	return Vector2(x / rhs, y / rhs);
}

Vector2& Vector2:: operator /= (const float rhs)
{
	this->x /= rhs;
	this->y /= rhs;
	return *this;
}

Vector2 DLL operator * (const float rhs, const Vector2& v)
{
	return Vector2(v.x * rhs, v.y * rhs);
}

Vector2 DLL operator / (const float rhs, const Vector2& v)
{
	return Vector2(v.x / rhs, v.y / rhs);
}