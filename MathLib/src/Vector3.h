
#pragma once
#include "DLL.h"
#include "Vector2.h"
class DLL Vector3
{
public:
	Vector3();
	Vector3(float x, float y, float z);
	~Vector3();
	bool operator == (const Vector3 rhs) const;

	Vector3 operator + (const Vector3 rhs) const; // This requires const as we are not changing the left hand side
	Vector3& operator += (const Vector3 rhs); // This requires reference as we are changing the original left hand side
	Vector3 operator - (const Vector3 rhs) const;
	Vector3& operator -= (const Vector3 rhs);
	Vector3 operator * (const float rhs) const;
	Vector3& operator *= (const float rhs);
	Vector3 operator / (const Vector3 rhs) const;
	Vector3& operator /= (const Vector3 rhs);
	Vector3 operator / (const float rhs) const;
	Vector3& operator /= (const float rhs);
	Vector3& operator = (const Vector3 rhs);
	float Dot(const Vector3 rhs) const;
	static float Dot(const Vector3& lhs, const Vector3& rhs);
	float Magnitude();
	Vector3& Normalise();
	Vector3 Normalised();
	Vector3 Cross(const Vector3 lhs) const;
	static Vector3 Cross(const Vector3& lhs, const Vector3& rhs);

	friend Vector3 DLL operator / (const float rhs, const Vector3& v);
	friend Vector3 DLL operator * (const float rhs, const Vector3& v);

	operator Vector2();

	float x, y, z;
private:

};


// VECTOR = VECTOR * FLOAT
// VECTOR 3 * MAT 4