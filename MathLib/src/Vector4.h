#pragma once
#include "DLL.h"

class DLL Vector4
{
public:
	Vector4();
	Vector4(float x, float y, float z, float w);

	float Dot(const Vector4 rhs) const;
	float x, y, z, w;

};

