#include "Matrix3.h"
#include <memory>

Matrix3::Matrix3()
{
	memset(m, 0, 36);
	m[0][0] = 1; m[1][1] = 1; m[2][2] = 1;
}


Matrix3::~Matrix3()
{
}

Vector3 Matrix3::RowToVec(const short r) const
{
	return Vector3(m[r][0], m[r][1], m[r][2]);
}


Vector3 Matrix3::ColToVec(const short c) const
{
	return Vector3(m[0][c], m[1][c], m[2][c]);
}

Matrix3 Matrix3::operator * (const Matrix3& rhs) const
{
	Matrix3 mat;
	mat.m[0][0] = RowToVec(0).Dot(rhs.ColToVec(0));
	mat.m[0][1] = RowToVec(0).Dot(rhs.ColToVec(1));
	mat.m[0][2] = RowToVec(0).Dot(rhs.ColToVec(2));

	mat.m[1][0] = RowToVec(1).Dot(rhs.ColToVec(0));
	mat.m[1][1] = RowToVec(1).Dot(rhs.ColToVec(1));
	mat.m[1][2] = RowToVec(1).Dot(rhs.ColToVec(2));

	mat.m[2][0] = RowToVec(2).Dot(rhs.ColToVec(0));
	mat.m[2][1] = RowToVec(2).Dot(rhs.ColToVec(1));
	mat.m[2][2] = RowToVec(2).Dot(rhs.ColToVec(2));

	return mat;
}

Matrix3& Matrix3::operator *= (const Matrix3& rhs)
{
	*this = *this * rhs;
	return *this;
}

Matrix3 Matrix3::CreateIdentity()
{
	Matrix3 mat;
	memset(mat.m, 0, 36);
	mat.m[0][0] = 1; mat.m[1][1] = 1; mat.m[2][2] = 1;
	return mat;
}
Matrix3 Matrix3::CreateRotation(float r)
{
	Matrix3 mat;
	mat.m[0][0] = cosf(r);
	mat.m[0][1] = -sinf(r);
	mat.m[1][0] = sinf(r);
	mat.m[1][1] = cosf(r);

	return mat;
}

Matrix3 Matrix3::CreateScale(float s)
{
	Matrix3 mat;
	mat.m[0][0] = s;
	mat.m[1][1] = s;
	return mat;
}

Matrix3 Matrix3::CreateTranslation(float x, float y)
{
	Matrix3 mat;
	mat.m[2][0] = x;
	mat.m[2][1] = y;

	return mat;
}

Vector2 operator * (const Vector2& v, const Matrix3& mat)
{
	Vector2 vec;
	vec.x = ((v.x * mat.m[0][0]) + (v.y * mat.m[1][0]));
	vec.y = ((v.x * mat.m[0][1]) + (v.y * mat.m[1][1]));
	return vec;
}

const float* Matrix3::GetPtr()
{
	return *m;
}