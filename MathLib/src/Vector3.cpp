#include "Vector3.h"
#include <math.h>

#pragma region Constructors
Vector3::Vector3()
{
	x = 0;
	y = 0;
	z = 0;
}

Vector3::Vector3(float x, float y, float z)
{
	this->x = x;
	this->y = y;
	this->z = z;
}
Vector3::~Vector3()
{
}
#pragma endregion

#pragma region Functions
bool Vector3::operator == (const Vector3 rhs) const
{
	return (x == rhs.x && y == rhs.y && this->z == z) ? true : false;
}

Vector3 Vector3::operator + (const Vector3 rhs) const
{
	return Vector3(this->x + rhs.x, this->y + rhs.y, this->z + rhs.z);
}

Vector3& Vector3::operator += (const Vector3 rhs)
{
	this->x += rhs.x;
	this->y += rhs.y;
	this->z += rhs.z;
	return *this;
}

Vector3 Vector3::operator - (const Vector3 rhs) const
{
	return Vector3(this->x - rhs.x, this->y - rhs.y, this->z - rhs.z);
}

Vector3& Vector3::operator -= (const Vector3 rhs)
{
	this->x -= rhs.x;
	this->y -= rhs.y;
	this->z -= rhs.z;
	return *this;
}

Vector3& Vector3::operator *= (const float rhs)
{
	this->x *= rhs;
	this->y *= rhs;
	this->z *= rhs;
	return *this;
}

Vector3 Vector3::operator * (const float rhs) const
{
	return Vector3(this->x * rhs, this->y * rhs, this->z * rhs);
}
Vector3 Vector3::operator/ (const Vector3 rhs) const
{
	return Vector3(this->x / rhs.x, this->y / rhs.y, this->z / rhs.z);
}

Vector3& Vector3::operator /= (const Vector3 rhs)
{
	this->x /= rhs.x;
	this->y /= rhs.y;
	this->z /= rhs.z;
	return *this;
}


Vector3& Vector3::operator /= (const float rhs)
{
	this->x /= rhs;
	this->y /= rhs;
	this->z /= rhs;
	return *this;
}

Vector3& Vector3::operator = (const Vector3 rhs)
{
	this->x = rhs.x;
	this->y = rhs.y;
	this->z = rhs.z;
	return *this;
}

//used for finding the angle between two vectors
float Vector3::Dot(const Vector3 rhs) const
{
	return ((this->x * rhs.x) + (this->y * rhs.y) + (this->z * rhs.z));
}

float Vector3::Dot(const Vector3& lhs, const Vector3& rhs)
{
	return ((lhs.x * rhs.x) + (lhs.y * rhs.y) + (lhs.z * rhs.z));
}


float Vector3::Magnitude()
{
	return sqrtf((x * x) + (y * y) + (z * z));
}

Vector3& Vector3::Normalise()
{
	float mag = Magnitude();
	x = x / mag;//what if mag is 0?
	y = y / mag;
	z = z / mag;

	return *this;
}

Vector3 Vector3::Normalised()
{
	Vector3 vec;
	float mag = Magnitude();
	vec.x = x / mag;//what if mag is 0?
	vec.y = y / mag;
	vec.z = z / mag;

	return vec;
}
//used to find a vector that keeps the two given vectors parallel to the plane
Vector3 Vector3::Cross(const Vector3 rhs) const
{
	return Vector3((this->y * rhs.z) - (this->z * rhs.y), (this->z * rhs.x) - (this->x * rhs.z), (this->x * rhs.y) - (this->y * rhs.x));
	
}

Vector3 Vector3::Cross(const Vector3& lhs, const Vector3& rhs)
{
	return Vector3((lhs.y * rhs.z) - (lhs.z * rhs.y), (lhs.z * rhs.x) - (lhs.x * rhs.z), (lhs.x * rhs.y) - (lhs.y * rhs.x));

}
#pragma endregion

#pragma region Global Functions
Vector3 DLL operator / (const float rhs, const Vector3& v)
{
	return Vector3(v.x / rhs, v.y / rhs, v.z / rhs);
}

Vector3 DLL operator * (const float rhs, const Vector3& v)
{
	return Vector3(v.x * rhs, v.y * rhs, v.z * rhs);
}
Vector3::operator Vector2()
{
	return Vector2(this->x, this->y);
}

#pragma endregion