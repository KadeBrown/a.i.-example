# README #

### What is this repository for? ###

Summary:
 This project was created in my first year of programming at the Academy of Interactive Entertainment. Its purpose is to demonstrate path-finding 
using dijkstra and a-star algorithms.

Requirements:
 In order to open or run this project, you must have Visual Studios 2013 installed.

### How do I get set up? ###

* To run this application, simply download and open the solution in Visual Studios 2013.

### Controls ###

* Use scroll wheel to select different tiles
* * Green Tile: Start Point of path
* * Red Tile: End Point of path
* * Grey Tile: Wall (Cannot be traversed)
* * Blue Tile: Water (has a higher cost of traversal)
* * Clear Tile: nothing
* Use left mouse click to place a tile on grid at your mouse position
* Once a start and end tile have been placed, press "enter" to find shortest path between them using dijkstra or "right shift" to use a-star
* Please note that the A.I. agents will sometimes go through walls if they have already found a path to their destination.

### Agent Types ###

* Normal Agents: these agents simply walk around until an agent with a gun comes near them at which point they will flee.
* Gun Agents: these agents will wander around until they spot a normal agent at which point they will seek that agent and proceed to shoot at them.
* Ambulance Agent: these agents will revive any normal agents that have been shot by a gun agent.