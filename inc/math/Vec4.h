#pragma once

class Vec4
{
public:
	Vec4();
	Vec4(float x, float y, float z, float w);

	float dot(const Vec4& v) const;
	float dotw(const Vec4& v) const;
	float mag() const;
	Vec4 normed() const;
	Vec4& norm();

	float x, y, z, w;
};

