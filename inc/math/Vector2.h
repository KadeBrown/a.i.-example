#pragma once
#include "DLL.h"

class DLL Vector2
{
public:
	Vector2();
	Vector2(float x, float y);

	float Dot(const Vector2 rhs) const;
	float Magnitude();
	Vector2& Normalise();
	Vector2 Normalised();

	static float Dot(const Vector2& lhs, const Vector2& rhs);

	Vector2& operator = (const Vector2 rhs);
	bool operator == (const Vector2 rhs) const;

	Vector2 operator + (const Vector2 rhs) const; // This requires const as we are not changing the left hand side
	Vector2& operator += (const Vector2 rhs); // This requires reference as we are changing the original left hand side
	Vector2 operator - (const Vector2 rhs) const;
	Vector2& operator -= (const Vector2 rhs);

	Vector2 operator * (const float rhs) const;
	Vector2& operator *= (const float rhs);
	Vector2 operator / (const float rhs) const;
	Vector2& operator /= (const float rhs);

	friend Vector2 DLL operator * (const float rhs, const Vector2& v);
	friend Vector2 DLL operator / (const float rhs, const Vector2& v);

	float x, y;	
};

