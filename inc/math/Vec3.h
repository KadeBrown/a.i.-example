#pragma once

class Vec3
{
public:
	Vec3();
	Vec3(float x, float y, float z);

	float dot(const Vec3& v) const;
	float mag() const;
	Vec3 normed() const;
	Vec3& norm();

	float x, y, z;
};

