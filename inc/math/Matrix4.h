#pragma once
#include "Vector4.h"
#include "Vector3.h"
#include "DLL.h"

class DLL Matrix4
{
public:
	Matrix4();
	~Matrix4();
	static Matrix4 CreateIdentity();
	static Matrix4 CreateScale(float s);
	static Matrix4 CreateRotationZ(float r);
	static Matrix4 CreateRotationX(float r);
	static Matrix4 CreateRotationY(float r);
	static Matrix4 CreateTranslation(float x, float y, float z);
	Matrix4 operator * (const Matrix4& rhs) const;
	Matrix4& operator *= (const Matrix4& rhs);
	const float* GetPtr();

	Vector4 RowToVec(const short r)const;
	Vector4 ColToVec(const short c)const;

	Matrix4 Transpose();
	friend DLL Vector3 operator * (const Vector3& v, const Matrix4& mat);
private:
	float m[4][4];
};

