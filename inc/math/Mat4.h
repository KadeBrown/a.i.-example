#pragma once
#include "Vec4.h"
#include <iostream>

class Mat4
{
public:
	Mat4();
	Mat4(float* arr);

	static Mat4 CreateScale(float scale);
	static Mat4 CreateRotationX(float theta);
	static Mat4 CreateRotationY(float theta);
	static Mat4 CreateRotationZ(float theta);
	static Mat4 CreateTranslation(float x, float y, float z);

	void GetArr(float* arr) const;
	const float* GetPtr() const;
	Vec4 RowToVec(short r) const;
	Vec4 ColToVec(short c) const;

	Mat4 operator * (const Mat4& o) const;
	void operator *= (const Mat4& o);

private:
	float m[4][4];
};