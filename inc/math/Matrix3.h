#pragma once
#include "Vector3.h"
#include "Vector2.h"
#include "DLL.h"

class DLL Matrix3
{
public:
	Matrix3();
	~Matrix3();

	static Matrix3 CreateIdentity();
	static Matrix3 CreateScale(float s);
	static Matrix3 CreateRotation(float r);
	static Matrix3 CreateTranslation(float x, float y);
	Matrix3 operator * (const Matrix3& rhs) const;
	Matrix3& operator *= (const Matrix3& rhs);
	const float* GetPtr();

	Vector3 RowToVec(const short r)const;
	Vector3 ColToVec(const short c)const;

	friend Vector2 operator * (const Vector2& v, const Matrix3& m);
private:
	float m[3][3];
};

