#pragma once
#include "Vec3.h"

class Mat3
{
public:
	Mat3();
	Mat3(float* arr);

	static Mat3 CreateScale(float scale);
	static Mat3 CreateRotation(float theta);
	static Mat3 CreateTranslation(float x, float y);

	void GetArr(float* arr) const;
	const float* GetPtr() const;
	Vec3 RowToVec(short r) const;
	Vec3 ColToVec(short c) const;

	Mat3 operator * (const Mat3& o) const;
	void operator *= (const Mat3& o);

private:
	float m[3][3];
};