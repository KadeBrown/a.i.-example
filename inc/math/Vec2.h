#pragma once

class Vec2
{
public:
	Vec2();
	Vec2(float x, float y);

	float dot(const Vec2& v) const;
	float mag() const;
	Vec2& norm();
	Vec2 normed() const;

	float x, y;
};