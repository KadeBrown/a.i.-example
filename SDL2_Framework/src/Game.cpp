#include "Game.h"
#include "SDL.h"
#include "SDL_Framework.h"
#include "Graph.h"
#include "Agent.h"
#include "AgentGun.h"
#include "AmbulanceAgent.h"

Game::Game()
{

}

Game::~Game()
{
	for (auto iter = objects.begin(); iter != objects.end(); ++iter)
	{
		delete (*iter);
	}
	objects.clear();
}

Game& Game::_Instance()
{
	static Game game;
	return game;
}

void Game::AddObject(GameObj* object)
{
	objects.push_back(object);
}

void Game::RemoveObject(GameObj* object)
{
	//adds game object to the list which will be deleted at start of each frame
	gmIter iter = std::find(toDelete.begin(), toDelete.end(), object);
	if (iter == toDelete.end())
		toDelete.push_back(object);
}

void Game::Render()
{
	//loops through all game objects and renders them
	for (gmIter iter = objects.begin(); iter != objects.end(); iter++)
	{
		(*iter)->Render();
	}
}

void Game::Update()
{
	//loops through all game objects and updates them
	for (gmIter iter = objects.begin(); iter != objects.end(); iter++)
	{
		GameObj* go = (*iter);
		go->Update();
	}
}

void Game::DeleteCondemnedObjects()
{
	//loop through all object to be deleted and delete them
	for (gmIter iter = toDelete.begin(); iter != toDelete.end(); iter++)
	{
		GameObj* objToDelete = (*iter);
		objects.remove(objToDelete);
		delete objToDelete;
	}
	toDelete.clear();
}

void Game::ReviveHealedAgents()
{
	Agent* temp;
	for (auto iter = toRevive.begin(); iter != toRevive.end(); iter++)
	{
		temp = (*iter);
		for (auto i = temp->agentsList.begin(); i != temp->agentsList.end(); ++i)
		{
			if ((*i) == nullptr)
				continue;
			else if ((*i) == temp)
			{
				i = temp->agentsList.erase(i);
			}
		}
		temp->agentsList.push_back(temp);
	}
	toRevive.clear();

}

void Game::Run()
{
	SYSTEM.Init("SDL2_Framework", 950, 950);
	float counter = 0;
	Node* start = nullptr;
	Node* end = nullptr;
	Node* nearestNode;
	std::list<Node*> path;

	selectedTile = Node::START;
	typeID = (int)Node::START;
	selectedTileColor = Color::green;

	//spawns a set amount of agents
	const int enemyCount = 15;
	for (int i = 0; i < enemyCount; i++)
		new Agent("BlueAgent.png");
	
	bool stop = false;
	//spawn one gun
	new AgentGun("Gun.png");
	//spawn one ambulance
	new AmbulanceAgent("BlueAgent.png");


	while (!stop)
	{
		//delete all agents that need to be deleted
		DeleteCondemnedObjects();
		//revive all healed agents
		ReviveHealedAgents();
		SYSTEM.Update();
		Update();

		SCREEN.BeginRendering();
		GRAPH.Render();
		GRAPH.RenderPath(path);
		Render();

		//scroll though tiles to place
		if (INPUTS.MouseScrollWheelDown() && selectedTile != Node::GROUND)
			++typeID;
		if (INPUTS.MouseScrollWheelUp() && selectedTile != Node::START)
			--typeID;

		selectedTile = (Node::NodeType)typeID;
		if (selectedTile == Node::START)
			selectedTileColor = Color::green;
		else if (selectedTile == Node::END)
			selectedTileColor = Color::red;
		else
			selectedTileColor = Node::TypeToColor(selectedTile);

		int x, y;
		Uint32 state = SDL_GetMouseState(&x, &y);
		nearestNode = GRAPH.FindNearestNode(Vector2((float)x - SCREEN.GetWidth() / 2, (float)-y + SCREEN.GetHeight() / 2));

		//place selected tile
		if (state & SDL_BUTTON(SDL_BUTTON_LEFT))
		{
			nearestNode->ChangeTileType(selectedTile);
			if (selectedTile == Node::START)
				start = nearestNode;
			if (selectedTile == Node::END)
				end = nearestNode;
		}

		if (INPUTS.GetKeyPressed(SDL_SCANCODE_RETURN) && start != nullptr && end != nullptr)
			path = GRAPH.DPath(start, end);
		if (INPUTS.GetKeyPressed(SDL_SCANCODE_RSHIFT) && start != nullptr && end != nullptr)
			path = GRAPH.APath(start, end);

		nearestNode->RenderColor(selectedTileColor);

		if (start != nullptr)
			start->RenderColor(Color::green);
		if (end != nullptr)
			end->RenderColor(Color::red);
		SCREEN.EndRendering();
	}

}
