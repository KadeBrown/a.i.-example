#include "AgentGun.h"
#include "Graph.h"
#include "Game.h"

AgentGun::AgentGun(const char* textureToLoad) : GameObj(textureToLoad)
{
	objType = GUN;
	standingNode = GRAPH.GetRandomNode();
	transform.scale = 0.1f;
	transform.pos = standingNode->position;

	gunsList.push_back(this);
}

AgentGun::~AgentGun()
{
	gunsList.remove(this);
}

void AgentGun::OnCollision(GameObj* other)
{
	if (other->objType == GameObj::AGENT)
	{
		GAME.RemoveObject(this);
	}
}