#include "GameObj.h"
#include "Game.h"
#include "Agent.h"
#include "AgentGun.h"
#include "AmbulanceAgent.h"

std::list<Agent*> GameObj::agentsList = std::list<Agent*>();
std::list<Agent*> GameObj::deadAgentsList = std::list<Agent*>();
std::list<AgentGun*> GameObj::gunsList = std::list<AgentGun*>();
std::list<AmbulanceAgent*> GameObj::ambulanceList = std::list<AmbulanceAgent*>();

GameObj::GameObj(const char* textureToLoad) : image(TEXBANK.Load(textureToLoad))
{
	objType = GUN;
	GAME.AddObject(this);
}

GameObj::~GameObj()
{
	transform.parent->RemoveChild(&transform);
}

void GameObj::Update()
{

}

void GameObj::Render()
{
	SCREEN.RenderTexture(image, transform.pos.x, transform.pos.y, transform.rotation, transform.scale, false, Color::white);
}

void GameObj::OnCollision(GameObj* other)
{

}

const Node* GameObj::GetStandingNode() const
{
	return standingNode;
}

