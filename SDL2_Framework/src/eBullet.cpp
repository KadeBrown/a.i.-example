#include "eBullet.h"
#include "Graph.h"
#include "Game.h"
#include "Agent.h"

eBullet::eBullet(Vector2 endPos, Vector2 startPos) : GameObj("circleBullet.png")
{
	startPosition = startPos;
	endPosition = endPos;
	transform.pos = startPosition;
	direction = (endPosition - startPosition).Normalised();
	speed = 750;
	transform.scale = 0.025f;
}

eBullet::~eBullet()
{
}

void eBullet::Update()
{
	transform.pos += direction * speed * DT;
	transform.Update();

	//have we hit a wall
	Node* currentNode = GRAPH.FindNearestNode(transform.pos);
	if (currentNode->type == Node::WALL)
		GAME.RemoveObject(this);
	// loop through agents
	for (auto i = agentsList.begin(); i != agentsList.end(); ++i)
	{
		//have we hit an agent without a gun
		if (currentNode == (*i)->GetStandingNode() && (*i)->hasGun == false)
		{
			//if so kill that agent and delete me
			deadAgentsList.push_back(*i);
			agentsList.remove(*i);
			GAME.RemoveObject(this);
			return;
		}
	}

	//are we out of bounds
	if (transform.pos.x < -1500)
		//if so delete me
		GAME.RemoveObject(this);
	else if (transform.pos.y < -2000)
		GAME.RemoveObject(this);
	else if (transform.pos.x > 1500)
		GAME.RemoveObject(this);
	else if (transform.pos.y > 2000)
		GAME.RemoveObject(this);
}

void eBullet::Render()
{
	SCREEN.RenderTexture(image, transform.GetLocalTransform().GetPtr());
}
