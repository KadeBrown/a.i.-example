#include "Transform.h"
#include "SDL_Framework.h"

Transform::Transform() : rotation(0), pos(0, 0), scale(1), children()
{
	parent = nullptr;
	GetRoot().AddChild(this);
}

Transform::Transform(bool root) : rotation(0), pos(0, 0), scale(1), children()
{
	parent = nullptr;
}

Transform::~Transform()
{
}

Transform& Transform::GetRoot()
{
	static Transform root(true);
	return root;
}

void Transform::RemoveChild(Transform* child)
{
	// if child is null, return
	if (child == nullptr)
		return;

	//find child in list of children
	std::vector<Transform*>::iterator iter = std::find(children.begin(), children.end(), child);
	//check if we have child
	if (iter != children.end())
	{
		//set child's parent to null
		child->parent = nullptr;
		//erase child
		children.erase(iter);
	}
}

void Transform::AddChild(Transform* child)
{
	//if child is null, return
	if (child == nullptr)
		return;
	//check if child exists 
	std::vector<Transform*>::iterator iter = std::find(children.begin(), children.end(), child);
	if (iter == children.end())
	{
		//add child to list of children
		children.push_back(child);
	}

	//check if child has parent
	if (child->parent != nullptr)
	{
		child->parent->RemoveChild(child);
	}
	//make this the childs parent
	child->parent = this;
}

Vector3 Transform::GetGlobalUp()
{
	Vector4 gUp = globalTransform.RowToVec(1);
	Vector3 _gUp(gUp.x, gUp.y, gUp.z);
	return _gUp;

}

Vector3 Transform::GetGlobalRight()
{
	Vector4 gRight = globalTransform.RowToVec(0);
	Vector3 _gRight(gRight.x, gRight.y, gRight.z);
	return _gRight;
}

Vector2 Transform::GetGlobalPosition()
{
	Vector4 gPos = globalTransform.RowToVec(3);
	Vector2 _gPos(gPos.x, gPos.y);
	return _gPos;
}

float Transform::GetGlobalScale()
{
	Vector4 scale = globalTransform.RowToVec(0);
	Vector3 _scale(scale.x, scale.y, scale.z);
	return _scale.Magnitude();

}

float Transform::GetGlobalRotation()
{
	//project (dot) global up against world up (0,1)
	Vector4 row = globalTransform.RowToVec(1);
	float dotUp = Vector3::Dot(Vector3(0, 1, 0), Vector3(row.x, row.y, row.z));
	//project (dot) global right against world up (0,1)
	row = globalTransform.RowToVec(0);
	float dotRight = Vector3::Dot(Vector3(0, 1, 0), Vector3(row.x, row.y, row.z));

	//if right less than 0
	//return cos(dot)
	if (dotRight < 0)
		return acosf(dotUp);
	//else
	else
		//return 2PI - cos(dot)
		return (2 * 3.1415) - acos(dotUp);



}

void Transform::Update()
{
	//calculate local transform by (s*r)*t
	localTransform =
		//create local scale matrix
		(Matrix4::CreateScale(scale) *
		//create local rotation matrix
		Matrix4::CreateRotationZ(rotation)) *
		//create local translation matrix
		Matrix4::CreateTranslation(pos.x * SCREEN.GetPixUnit(), pos.y * SCREEN.GetPixUnit(), 0 * SCREEN.GetPixUnit());

	//if parent exists
	if (parent != nullptr)
	{
		//global is local * parent.global
		globalTransform = localTransform * parent->GetGlobalTransform();
	}
	//else
	else
	{
		//global is local
		globalTransform = localTransform;
	}

	//loop through children
	for (int i = 0; i < children.size(); ++i)
	{
		//update children
		children[i]->Update();
	}
}