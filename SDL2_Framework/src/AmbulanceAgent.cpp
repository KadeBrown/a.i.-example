#include "AmbulanceAgent.h"
#include "Graph.h"
#include "Agent.h"

AmbulanceAgent::AmbulanceAgent(const char* textureToLoad) : GameObj(textureToLoad)
{
	movementSpeed = 100.0f;
	transform.scale = 0.1f;
	textureCounter = 0;

	standingNode = GRAPH.FindNearestNode(transform.pos);
	behaviourManager = new A_BehaviourManager(this);
	behaviourManager->ChangeBehaviour(A_BehaviourManager::WANDER);

	ambulanceList.push_back(this);
}

AmbulanceAgent::~AmbulanceAgent()
{
	delete this;
}

void AmbulanceAgent::SetTarget(GameObj* newTarget)
{
	target = newTarget;
}

void AmbulanceAgent::ChangeBehavior(A_BehaviourManager::A_Behaviour newBehavior)
{
	behaviourManager->ChangeBehaviour(newBehavior);
}

void AmbulanceAgent::ReturnToPreviousBehavior()
{
	behaviourManager->ReturnToPreviousBehaviour();
}

Agent* AmbulanceAgent::FindClosestDeadAgent()
{
	Agent* closestAgent = nullptr;
	float distance = INF;
	//loop through all the dead agents
	for (auto i = deadAgentsList.begin(); i != deadAgentsList.end(); ++i)
	{
		//is it closer than the previous
		if (distance > (transform.pos - (*i)->transform.pos).Magnitude())
		{
			//new closest
			distance = (transform.pos - (*i)->transform.pos).Magnitude();
			closestAgent = (*i);
		}

	}
	return closestAgent;
}

GameObj* AmbulanceAgent::GetTarget()
{
	return target;
}

void AmbulanceAgent::Update()
{
	behaviourManager->Update();
}

void AmbulanceAgent::Render()
{
	textureCounter += 0.5f;

	if (textureCounter < 15)
		SCREEN.RenderTexture(TEXBANK.Load("Ambulance1.png"), transform.pos.x, transform.pos.y, transform.rotation, transform.scale, false, Color::white);
	else if (textureCounter > 15 && textureCounter < 20)
	{
		SCREEN.RenderTexture(TEXBANK.Load("Ambulance2.png"), transform.pos.x, transform.pos.y, transform.rotation, transform.scale, false, Color::white);
	}
	else if (textureCounter > 20)
	{
		SCREEN.RenderTexture(TEXBANK.Load("Ambulance1.png"), transform.pos.x, transform.pos.y, transform.rotation, transform.scale, false, Color::white);
		textureCounter = 0;
	}
	else
		SCREEN.RenderTexture(TEXBANK.Load("Ambulance1.png"), transform.pos.x, transform.pos.y, transform.rotation, transform.scale, false, Color::white);
}