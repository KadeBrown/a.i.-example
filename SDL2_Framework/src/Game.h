#pragma once
#include "framework\SDL_Framework.h"
#include "Node.h"
#include "GameObj.h"
#include <list>

#define GAME (Game::_Instance())
typedef std::list<GameObj*>::iterator gmIter;
class Agent;

class Game
{
public:
	static Game& _Instance();

	/*
	Description: adds a game object to the list of game objects
	Param: the game object you would like to add
	*/
	void AddObject(GameObj* object);

	/*
	Description: removes a game object from the list of game objects
	Param: the game object you would like to remove
	*/
	void RemoveObject(GameObj* object);

	/*
	Description: renders all game objects
	*/
	void Render();

	/*
	Description: updates all game objects
	*/
	void Update();

	/*
	Description: the main while loop where most functions are being called from
	*/
	void Run();

	/*
	Description: revives all the agents that have been healed
	*/
	void ReviveHealedAgents();

	static void OnQuit();
	static void OnQuit(SDL_Event e);

	std::list<GameObj*> objects;
	std::list<GameObj*> toDelete;
	std::list<Agent*> toRevive;
	int typeID;
	Node::NodeType selectedTile;
	Color selectedTileColor;

private:
	Game();
	~Game();

	void DeleteCondemnedObjects();

	Game(const Game&) = delete;
	void operator = (const Game&) = delete;
};


