#include "Graph.h"
#include "Edge.h"
#include "Node.h"

Graph& Graph::GetInstance()
{
	static Graph graph("Tile.png", Color::blue, 32, 33);
	return graph;
}

Graph::Graph(std::string nodeTexName, Color edgeColor, float nodeFrequency, float linkRadius)
{
	this->nodeTexName = nodeTexName;
	this->edgeColor = edgeColor;
	this->nodeFrequency = nodeFrequency;
	this->linkRadius = linkRadius;

	Generate();
}

Graph::~Graph()
{
	//delete all nodes and there edges
	for (std::vector<Node*>::iterator iter = nodes.begin(); iter != nodes.end(); ++iter)
	{
		for (std::vector<Edge*>::iterator iters = (*iter)->edges.begin(); iters != (*iter)->edges.end(); ++iters)
		{
			delete *iters;
		}
		delete *iter;
	}
}

Node* Graph::FindNearestNode(Vector2 position)
{
	Node* closest = nullptr;
	auto first = nodes.begin();
	float distance = (position - (*first)->position).Magnitude();

	//loop through all nodes and find if one is closer
	for (auto iter = nodes.begin(); iter != nodes.end(); ++iter)
	{
		if (distance > (position - (*iter)->position).Magnitude())
		{
			distance = (position - (*iter)->position).Magnitude();
			closest = (*iter);
		}
	}
	//if there is no closest, set the closest to the first node
	if (closest == nullptr)
			closest = (*first);
	return closest;
}

void Graph::Render()
{
	for (std::vector<Node*>::iterator iter = nodes.begin(); iter != nodes.end(); ++iter)
	{
		(*iter)->Render();	
		for (std::vector<Edge*>::iterator iters = (*iter)->edges.begin(); iters != (*iter)->edges.end(); ++iters)
		{
			(*iters)->Render();
		}
	}
}

void Graph::Generate()
{
	// Calculate the height and width of the window
	float screenWidth = SCREEN.GetWidth() - TEXBANK.Load(nodeTexName).GetWidth();
	float screenHeight = SCREEN.GetHeight() - TEXBANK.Load(nodeTexName).GetHeight();

	//calculate how many times you can fit the given texture on the screen
	float xCount = (-screenWidth * 0.5f) + TEXBANK.Load(nodeTexName).GetWidth() * 0.12f;
	float yCount = (screenHeight * 0.5f) - TEXBANK.Load(nodeTexName).GetHeight() * 0.12f;

	//loop until you cant fit anymore textures
	for  (int i = 0;  i < screenWidth / nodeFrequency; i++)
	{
		for (int s = 0; s < screenHeight / nodeFrequency; s++)
		{
			nodes.push_back(new Node("Tile.png",Vector2(xCount, yCount))); 
			xCount += nodeFrequency;
		}
		yCount -= nodeFrequency;
		xCount = (-screenWidth * 0.5f) + TEXBANK.Load(nodeTexName).GetWidth() * 0.12f;
	}

	//Generate the Edges
	for (std::vector<Node*>::iterator iter = nodes.begin(); iter != nodes.end(); ++iter)
	{
		for (std::vector<Node*>::iterator iters = nodes.begin(); iters != nodes.end(); ++iters)
		{
			if ((*iter) != (*iters) && ((*iter)->position - (*iters)->position).Magnitude() <= linkRadius)
			{			
				(*iter)->edges.push_back(new Edge(*iter, *iters, Color::blue));
			}
		}
	}

	start = nodes[rand() % nodes.size()];
	end = nodes[rand() % nodes.size()];
	while (start == end)
	{
		end = nodes[rand() % nodes.size()];
	}
}

void Graph::Insert(Node* node, bool astar)
{
	if (openList.size() == 0)
	{
		openList.push_back(node);
		return;
	}

	//is the node already added to the list
	std::list<Node*>::iterator i = std::find(closedList.begin(), closedList.end(), node);
	if (i != closedList.end())
		return;

	for (std::list<Node*>::iterator iter = openList.begin(); iter != openList.end(); ++iter)
	{
		if (( astar && node->fScore <= (*iter)->fScore) ||
			(!astar && node->gScore <= (*iter)->gScore))
		{
			openList.insert(iter, node);
			return;
		}
	}

	openList.push_back(node);
}

std::list<Node*> Graph::DPath(Node* start, Node* end)
{
	//clear all lists
	openList.clear();
	closedList.clear();
	this->start = start;
	this->end = end;

	//add start node
	start->nScore = nullptr;
	start->gScore = 0;
	openList.push_back(start);
	
	while (openList.size() != 0)
	{
		//pull off cheapest node
		Node* node = *openList.begin();
		openList.pop_front();
		closedList.push_back(node);

		//check if end node...
		if (node == end)
			return CreatePath(end);

		//process edges
		for (std::vector<Edge*>::iterator iter = node->edges.begin(); iter != node->edges.end(); ++iter)
		{
			Edge* edge = (*iter);
			Node* to = edge->to;
			float gScore = node->gScore + (edge->cost * to->terrainCost);
			
			//if 'to' node is on closed list
			if (std::find(closedList.begin(), closedList.end(), to) != closedList.end())
				//continue/skip node
				continue;

			if (to->type != Node::WALL)
			{
				auto io = std::find(openList.begin(), openList.end(), to);
				//if on open list
				if (io != openList.end())
				{
					//if can get there cheaper
					if (to->gScore > gScore)
					{
						//remove from list
						openList.erase(io);
						//change nScore
						to->nScore = node;
						//change gScore
						to->gScore = gScore;
						//insert to open list
						Insert(to, false);
					}
				}
				//else (it's not on either list)
				else
				{
					//set nScore
					to->nScore = node;
					//set gScore
					to->gScore = gScore;
					//insert to open list
					Insert(to, false);
				}
			}
		}
	}

	return std::list<Node*>();
}

std::list<Node*> Graph::APath(Node* start, Node* end)
{
	openList.clear();
	closedList.clear();
	this->start = start;
	this->end = end;

	//add start node
	start->nScore = nullptr;
	start->gScore = 0;
	//TODO: set h and f scores
	start->hScore = 0;
	start->fScore = 0;
	openList.push_back(start);

	while (openList.size() != 0)
	{
		//pull off cheapest node
		Node* node = *openList.begin();
		openList.pop_front();
		closedList.push_back(node);

		//check if end node...
		if (node == end)
			return CreatePath(end);

		//process edges
		for (std::vector<Edge*>::iterator iter = node->edges.begin(); iter != node->edges.end(); ++iter)
		{
			Edge* edge = (*iter);
			Node* to = edge->to;
			float gScore = node->gScore + (edge->cost * to->terrainCost);
			//hScore = the distance between a given node and the end node
			float hScore = (to->position - end->position).Magnitude();
			//fScore = how much it would cost to get to here + how much more it would cost to get from here to the end node
			float fScore = gScore + node->hScore;
			//if 'to' node is on closed list
			if (std::find(closedList.begin(), closedList.end(), to) != closedList.end())
				//continue/skip node
				continue;

			if (to->type != Node::WALL)
			{
				auto io = std::find(openList.begin(), openList.end(), to);
				//if on open list
				if (io != openList.end())
				{
					//if can get there cheaper
					if (to->fScore > fScore) 
					{
						//remove from list
						openList.erase(io);
						//change nScore
						to->nScore = node;
						//change gScore
						to->gScore = gScore;
						//TODO: set h and f scores
						to->fScore = fScore;
						to->hScore = hScore;
						//insert to open list
						Insert(to, true);
					}
				}
				//else (it's not on either list)
				else
				{
					//set nScore
					to->nScore = node;
					//set gScore
					to->gScore = gScore;
					to->fScore = fScore;
					to->hScore = hScore;
					//insert to open list
					Insert(to, true);
				}
			}
		}
	}
	return std::list<Node*>();
}

std::list<Node*> Graph::CreatePath(Node* end)
{
	//make list to hold path
	std::list<Node*> path;
	//make node* "current" for iterating through nodes in path (begins at end)
	Node* current = end;
	//while not start node
	while (current != nullptr)
	{
		//add current node's position to list
		path.push_front(current);
		//set current to next node
		current = current->nScore;
	}
	//return path
	return path;
}

void Graph::RenderPath(std::list<Node*>& path)
{
	for (auto iter = path.begin(); iter != path.end(); ++iter)
	{
		(*iter)->RenderColor(Color::blue);
	}
}

Node* Graph::GetRandomNode()
{
	Node* randomNode = nodes[rand() % nodes.size()];
	while (randomNode->type == Node::WALL)
	{
		randomNode = nodes[rand() % nodes.size()];
	}
	return randomNode;
}
