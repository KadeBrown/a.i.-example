#include "IBehavior.h"
#include "Agent.h"
#include "Node.h"
#include "Graph.h"
#include <math.h>
#include "Game.h"
#include "AgentGun.h"
#include "eBullet.h"
#include "AmbulanceAgent.h"
#include "GameObj.h"

BehaviourManager::BehaviourManager(Agent* target)
{
	this->target = target;
	currentBehaviour.behaviourType = SEEK;
	currentBehaviour.behaviourAction = new Seek();
	previousBehaviour.behaviourType = WANDER;
	previousBehaviour.behaviourAction = nullptr;
}

BehaviourManager::~BehaviourManager()
{
	if (currentBehaviour.behaviourAction != nullptr)
		delete currentBehaviour.behaviourAction;
	if (previousBehaviour.behaviourAction != nullptr)
		delete previousBehaviour.behaviourAction;
}

BehaviourManager::Behaviour BehaviourManager::GetCurrentBehaviour()
{
	return currentBehaviour.behaviourType;
}

void BehaviourManager::ReturnToPreviousBehaviour()
{
	if (previousBehaviour.behaviourAction != nullptr)
	{
		// If there is a current behavior. Delete that mother.... Some memory management!
		if (currentBehaviour.behaviourAction != nullptr)
		{
			delete currentBehaviour.behaviourAction;
		}

		// Set Current Behavior to Previous Behavior
		currentBehaviour.behaviourAction = previousBehaviour.behaviourAction;
		currentBehaviour.behaviourType = previousBehaviour.behaviourType;
		currentBehaviour.behaviourAction->Reset();

		// No Previous Behavior Exists!
		previousBehaviour.behaviourAction = nullptr;
	}
}

void BehaviourManager::ChangeBehaviour(Behaviour changeTo)
{
	// We are about to replace the previous Behaviour. So delete it from memory.
	if (previousBehaviour.behaviourAction != nullptr)
	{
		delete previousBehaviour.behaviourAction;
	}

	// Set Previous Behavior to current Behavior
	previousBehaviour.behaviourAction = currentBehaviour.behaviourAction;
	previousBehaviour.behaviourType = currentBehaviour.behaviourType;

	// Change Current Behavior to what is desired
	currentBehaviour.behaviourType = changeTo;
	currentBehaviour.behaviourAction = GetNewBehaviour(changeTo);
}

void BehaviourManager::Update()
{
	currentBehaviour.behaviourAction->Update(target);
}

void BehaviourManager::Render()
{
	currentBehaviour.behaviourAction->Render();
}

IBehaviour* BehaviourManager::GetNewBehaviour(Behaviour changeTo)
{
	switch (changeTo)
	{
	case BehaviourManager::SEEK:
		return new Seek();
	case BehaviourManager::FLEE:
		return new Flee();
	default:
		return new Wander();
	}
}

//IBehaviour
void IBehaviour::Reset()
{
	pathList.clear();
}

void IBehaviour::FollowPath(Agent* agent)
{
	if (pathList.empty())
		return;

	Node* waypoint = pathList.front();
	Vector2 direction = (waypoint->position - agent->transform.pos).Normalised();
	agent->transform.pos += direction * agent->movementSpeed * DT;
	agent->transform.rotation = atan2f(direction.x, direction.y);


	if (GRAPH.FindNearestNode(agent->transform.pos) == waypoint)
	{
		agent->standingNode = pathList.front();
		pathList.pop_front();
	}
}

//Wander
void Wander::Update(Agent* agent)
{
	for (auto i = agent->deadAgentsList.begin(); i != agent->deadAgentsList.end(); ++i)
	{
		if ((*i) == agent)
			return;
	}
	agent->movementSpeed = 125.0f;
	float distance = 35.0f * 5;

	
	if (agent->hasGun == true)
	{
		Agent* closestNonThreat = agent->FindClosestNonThreat();
		if (closestNonThreat != nullptr && (closestNonThreat->transform.pos - agent->transform.pos).Magnitude() < distance)
		{
			agent->SetTarget(closestNonThreat);
			agent->ChangeBehavior(BehaviourManager::SEEK);
			return;
		}
	}

	else
	{
		Agent* closestThreat = agent->FindClosestThreat();
		//if there is a threat near us
		if (closestThreat != nullptr && (agent->transform.pos - closestThreat->transform.pos).Magnitude() < distance)
		{
			//run away
			agent->ChangeBehavior(BehaviourManager::FLEE);
			return;
		}
	}

	
	AgentGun* closestGun = agent->FindClosestGun();
	//if there is a gun near us
	if (closestGun != nullptr)
	{
		if (distance > (agent->transform.pos - closestGun->transform.pos).Magnitude())
		{	
			//go get it
			agent->SetTarget(closestGun);
			agent->ChangeBehavior(BehaviourManager::SEEK);
			return;
		}
	}



	// Make Path if none exist
	while (pathList.empty())
	{
		Node* startNode = GRAPH.FindNearestNode(agent->transform.pos);		// Get The Node The Agent Is In. This is where we start from
		Node* endNode = GRAPH.GetRandomNode();
		pathList = GRAPH.APath(startNode, endNode);
	}

	// Follow Path
	FollowPath(agent);
}

void Wander::Render()
{
	//GRAPH.RenderPath(pathList);
}

//Flee
Flee::Flee()
{
	Timer = 0;
}

void Flee::Update(Agent* agent)
{
	//am i dead
	for (auto i = agent->deadAgentsList.begin(); i != agent->deadAgentsList.end(); ++i)
	{
		//if so, we cant do anything
		if ((*i) == agent)
			return;
	}

	agent->movementSpeed = 150.0f;
	Vector2 targetPos;
	Node* targetnode;
	Agent* closestAgent = agent->FindClosestThreat();
	float distance = 35.0f * 5;

	
	// Not in danger?
	if (closestAgent == nullptr || (agent->transform.pos - closestAgent->transform.pos).Magnitude() > distance + 100)
	{
		agent->ChangeBehavior(BehaviourManager::WANDER);
		return;
	}

	//get enemies position and direction and add it to our own hence moving away from them
	targetPos = (agent->transform.pos);
	targetPos += (agent->transform.pos - closestAgent->transform.pos).Normalised() * 320.0f;

	//update path every 0.5 seconds
	Timer += DT;
	if (Timer > 0.5f)
	{
		pathList.clear();
		Timer = 0;
	}

	//if path list is empty, make a new one
	if (pathList.empty())  
	{
		Node* startNode = GRAPH.FindNearestNode(agent->transform.pos);
		Node* endNode = GRAPH.FindNearestNode(targetPos);
		if (startNode == endNode)
			endNode = GRAPH.GetRandomNode();
		pathList = GRAPH.APath(startNode, endNode);
	}
	FollowPath(agent);
}

//Seek
Seek::Seek()
{
	currentTime = 0;
}

void Seek::Update(Agent* agent)
{
	for (auto i = agent->deadAgentsList.begin(); i != agent->deadAgentsList.end(); ++i)
	{
		if ((*i) == agent)
			return;
	}
	agent->movementSpeed = 85.0f;

	currentTime += DT;
	if (currentTime > 2)
	{
		if (agent->hasGun == false)
		{
			agent->SetTarget(agent->FindClosestGun());
		}
		else
		{
			agent->SetTarget(agent->FindClosestNonThreat());
		}
		pathList.clear();
		currentTime = 0;
	}

	float distance = 35.0f * 5;
	
	// CHeck for FLEE \(._.)/ Only if I gotz no pewpew
	Agent* threat = agent->FindClosestThreat();
	if (agent->hasGun == false && threat != nullptr && (agent->transform.pos - threat->transform.pos).Magnitude() < distance)
	{
		agent->ChangeBehavior(BehaviourManager::FLEE);
		return;
	}

	// Check for Target to Snipe (~.^). Exit behaviour if NONE
	if (agent->GetTarget() == nullptr || (agent->GetTarget()->transform.pos - agent->transform.pos).Magnitude() > distance + 100)
	{
		// Can't get to Target ~ AGENT || GUN
		agent->ChangeBehavior(BehaviourManager::WANDER);
		return;
	}

	// Find Path to target. Do IT SON!
	if (pathList.empty())
	{
		Node* startNode = GRAPH.FindNearestNode(agent->transform.pos);		// Get The Node The Agent Is In. This is where we start from
		Node* endNode = GRAPH.FindNearestNode(agent->GetTarget()->transform.pos);
		pathList = GRAPH.APath(startNode, endNode);

		// Still Empty? D: Guess we can't get there. Can't do this behaviour! ;_;
		if (pathList.empty())
		{
			agent->ChangeBehavior(BehaviourManager::WANDER);
			return;
		}
	}
		


	if (agent->GetTarget()->objType == GameObj::AGENT)
	{
		if ((agent->GetTarget()->transform.pos - agent->transform.pos).Magnitude() < distance + 100)
		{
			if (agent->hasGun == true)
			{
				agent->shootTimer += DT;
				if (agent->shootTimer > 2)
				{
					new eBullet(agent->GetTarget()->transform.pos, agent->transform.pos);
					agent->shootTimer = 0;
				}
			}
			FollowPath(agent);
			return;
		}
	}
	else
	{
		FollowPath(agent);
	}	
}


