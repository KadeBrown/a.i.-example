#pragma once
#include "GameObj.h"
#include "HealerBehaviours.h"

class AmbulanceAgent : public GameObj
{
public:
	AmbulanceAgent(const char* textureToLoad);
	~AmbulanceAgent();

	/*
	Description: sets target to a given game object
	Param: game object you would like to target
	*/
	void SetTarget(GameObj* target);

	/*
	Description: changes current behaviour to a given behaviour
	Param: behaviour you would like to change to
	*/
	void ChangeBehavior(A_BehaviourManager::A_Behaviour newBehavior);

	/*
	Description: sets current behaviour back to previous behaviour
	*/
	void ReturnToPreviousBehavior();

	/*
	Description: finds closest dead agent
	Returns: closest dead agent
	*/
	Agent* FindClosestDeadAgent();

	/*
	Returns: returns target
	*/
	GameObj* GetTarget();

	/*
	Description: updates position and behaviour state
	*/
	void Update();
	void Render();

	float movementSpeed;
	float textureCounter;
private:
	A_BehaviourManager* behaviourManager;
	GameObj* target;
};

