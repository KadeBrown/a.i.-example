#pragma once
#include <list>

class Agent;
class HealerBehaviours;
class Node;
class AmbulanceAgent;

class A_BehaviourManager
{
public:
	A_BehaviourManager(AmbulanceAgent* target);
	~A_BehaviourManager();

	enum A_Behaviour
	{
		SEEK,
		WANDER,
		HEAL
	};

	/*
	Returns: returns current behaviour
	*/
	A_Behaviour GetCurrentBehaviour();

	/*
	Description: set the current behaviour to the previous behaviour
	*/
	void ReturnToPreviousBehaviour();

	/*
	Description: sets the current behaviour to a given behaviour
	Param: the behaviour you wish to change to
	*/
	void ChangeBehaviour(A_Behaviour changeTo);

	/*
	Description: updates the current behaviour
	*/
	void Update();
	void Render();


private:

	struct A_BehaviourHandler
	{
		A_Behaviour behaviourType;			// The identity of the action I am undertaking.
		HealerBehaviours* behaviourAction;		// The instance of the action itself.
	};

	A_BehaviourHandler currentBehaviour;
	A_BehaviourHandler previousBehaviour;
	AmbulanceAgent* target;

	/*
	Description: changes the agents behaviour to a given behaviour
	Param: the behaviour action you wish to change to
	Returns: a class instance of the specified behaviour
	*/
	HealerBehaviours* GetNewBehaviour(A_Behaviour changeTo);
};


class HealerBehaviours
{
public:
	HealerBehaviours() {};
	virtual ~HealerBehaviours() {};

	/*
	Description: clears the path list
	*/
	virtual void Reset();

	virtual void Update(AmbulanceAgent* agent) {}
	virtual void Render() {}

protected:

	/*
	Description: moves the given agent along the path
	Param: the agent you want to move
	*/
	void FollowPath(AmbulanceAgent* agent);
	std::list<Node*> pathList;
};



class A_Wander : public HealerBehaviours
{
public:

	/*
	Description: finds a random node and creates a path to that node
	Param: the agent you want to folow this behaviour
	*/
	void Update(AmbulanceAgent* agent);
	void Render();
};



class A_Seek : public HealerBehaviours
{
public:
	A_Seek();

	/*
	Description: seeks to the agents target
	Param: the agent you want to follow this behaviour
	*/
	void Update(AmbulanceAgent* agent);

private:
	float currentTime;
};



class A_Heal : public HealerBehaviours
{
public:

	A_Heal();

	/*
	Description: heals the closest dead agent
	Param: the agent you want to follow this behaviour
	*/
	void Update(AmbulanceAgent* agent);

private:
	float healTime;
};

