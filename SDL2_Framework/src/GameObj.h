#pragma once
#include "Transform.h"
#include <iostream>
#include "SDL.h"
#include "SDL_Framework.h"
#include <list>

class Agent;
class AgentGun;
class Node;
class AmbulanceAgent;

class GameObj
{
public:
	GameObj(const char* textureToLoad);
	virtual ~GameObj();


	virtual void Update();
	virtual void Render();
	virtual void OnCollision(GameObj* other);

	/*
	Returns: returns standing node (node we are currently standing on)
	*/
	const Node* GetStandingNode() const;
	
	enum ObjectType
	{
		AGENT,
		COIN,
		GUN,
	};

	Transform transform;
	Texture image;
	ObjectType objType;

	static std::list<Agent*> agentsList;
	static std::list<Agent*> deadAgentsList;
	static std::list<AgentGun*> gunsList;
	static std::list<AmbulanceAgent*> ambulanceList;
protected:
	friend class IBehaviour;
	friend class HealerBehaviours;

	Node* standingNode;
};
