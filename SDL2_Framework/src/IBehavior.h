#pragma once
#include <list>

class Agent;
class IBehaviour;
class Node;
class AmbulanceAgent;

class BehaviourManager
{
public:
	BehaviourManager(Agent* target);

	~BehaviourManager();

	enum Behaviour
	{
		SEEK,
		FLEE,
		WANDER,
		HEAL
	};

	Behaviour GetCurrentBehaviour();
	void ReturnToPreviousBehaviour();
	void ChangeBehaviour(Behaviour changeTo);
	void Update();
	void Render();


private:

	struct BehaviourHandler
	{
		Behaviour behaviourType;			// The identity of the action I am undertaking.
		IBehaviour* behaviourAction;		// The instance of the action itself.
	};

	BehaviourHandler currentBehaviour;
	BehaviourHandler previousBehaviour;
	Agent* target;

	IBehaviour* GetNewBehaviour(Behaviour changeTo);
};


class IBehaviour
{
public:
	IBehaviour() {};
	virtual ~IBehaviour() {};

	virtual void Reset();
	virtual void Update(Agent* agent) {}

	virtual void Render() {}

protected:
	void FollowPath(Agent* agent);

	std::list<Node*> pathList;
};

class Wander : public IBehaviour
{
public:
	void Update(Agent* agent);

	void Render();
};

class Flee : public IBehaviour
{
public:
	Flee();

	/*
	Description: agent runs away from the closest threat is one exists
	Param: the agent you want to follow this behaviour
	*/
	void Update(Agent* agent);

private:
	float Timer;
};

class Seek : public IBehaviour
{
public:
	Seek();

	void Update(Agent* agent);

private:
	float currentTime;
};


