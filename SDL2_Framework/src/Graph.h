#pragma once
#include <vector>
#include <string>
#include "SDL_Framework.h"
#include "Node.h"
#include <list>

#define GRAPH Graph::GetInstance()

class Graph
{
public:
	Graph(std::string nodeTexName, Color edgeColor, float nodeFrequency, float linkRadius);
	~Graph();

	static Graph& GetInstance();

	/*
	Description: finds the closest node to a given position
	Param: position you wish to find the closest node to
	Returns: the node closest to the given position
	*/
	Node* FindNearestNode(Vector2 position);

	/*
	Description: dikjsrta pathfinding algorithm
	Param: node to start from and node to end at
	Returns: a list of nodes that make up the shortest path between the start and end nodes
	*/
	std::list<Node*> DPath(Node* start, Node* end);

	/*
	Description: A* pathfinding algorithm
	Param: node to start from and node to end at
	Returns: a list of nodes that make up the shortest path between the start and end nodes
	*/
	std::list<Node*> APath(Node* start, Node* end);
	void Render();

	/*
	Description: renders all the nodes in the path a different color
	Param: a list of nodes that make up the path
	*/
	void RenderPath(std::list<Node*>& path);

	/*
	Description: gets a random node that is not a wall
	Returns: a random node that is not a node
	*/
	Node* GetRandomNode();

private:
	/*
	Description: generates all the nodes and edges
	*/
	void Generate();

	/*
	Description: finds the appropriate location and adds the node to the open list
	Param: the node you wish to add and whether you are using A* or not
	*/
	void Insert(Node* node, bool astar);

	/*
	Description: creates a path to a given end node
	Param: the node you wish to create a path to
	Returns: a list of node that make up a path to the end node
	*/
	std::list<Node*> CreatePath(Node* end);

	std::vector<Node*> nodes;
	std::list<Node*> openList;
	std::list<Node*> closedList;
	std::string nodeTexName;
	Node* start, *end;
	Color edgeColor;
	float nodeFrequency, linkRadius;
	
};

