#pragma once
#include "GameObj.h"

class AgentGun : public GameObj
{
public:
	AgentGun(const char* textureToLoad);
	~AgentGun();

	/*
	Description: destroys itself
	Param: game object we have colided with
	*/
	void OnCollision(GameObj* other);
};

