#include "HealerBehaviours.h"
#include "Agent.h"
#include "Node.h"
#include "Graph.h"
#include <math.h>
#include "Game.h"
#include "AgentGun.h"
#include "eBullet.h"
#include "AmbulanceAgent.h"
#include "GameObj.h"

A_BehaviourManager::A_BehaviourManager(AmbulanceAgent* target)
{
	this->target = target;
	currentBehaviour.behaviourType = SEEK;
	currentBehaviour.behaviourAction = new A_Seek();
	previousBehaviour.behaviourType = WANDER;
	previousBehaviour.behaviourAction = nullptr;
}

A_BehaviourManager::~A_BehaviourManager()
{
	if (currentBehaviour.behaviourAction != nullptr)
		delete currentBehaviour.behaviourAction;
	if (previousBehaviour.behaviourAction != nullptr)
		delete previousBehaviour.behaviourAction;
}

A_BehaviourManager::A_Behaviour A_BehaviourManager::GetCurrentBehaviour()
{
	return currentBehaviour.behaviourType;
}

void A_BehaviourManager::ReturnToPreviousBehaviour()
{
	if (previousBehaviour.behaviourAction != nullptr)
	{
		// If there is a current behavior. Delete it
		if (currentBehaviour.behaviourAction != nullptr)
		{
			delete currentBehaviour.behaviourAction;
		}

		// Set Current Behavior to Previous Behavior
		currentBehaviour.behaviourAction = previousBehaviour.behaviourAction;
		currentBehaviour.behaviourType = previousBehaviour.behaviourType;
		currentBehaviour.behaviourAction->Reset();

		// No Previous Behavior Exists!
		previousBehaviour.behaviourAction = nullptr;
	}
}

void A_BehaviourManager::ChangeBehaviour(A_Behaviour changeTo)
{
	// We are about to replace the previous Behaviour. So delete it from memory.
	if (previousBehaviour.behaviourAction != nullptr)
	{
		delete previousBehaviour.behaviourAction;
	}

	// Set Previous Behavior to current Behavior
	previousBehaviour.behaviourAction = currentBehaviour.behaviourAction;
	previousBehaviour.behaviourType = currentBehaviour.behaviourType;

	// Change Current Behavior to what is desired
	currentBehaviour.behaviourType = changeTo;
	currentBehaviour.behaviourAction = GetNewBehaviour(changeTo);
}

void A_BehaviourManager::Update()
{
	currentBehaviour.behaviourAction->Update(target);
}

void A_BehaviourManager::Render()
{
	currentBehaviour.behaviourAction->Render();
}

HealerBehaviours* A_BehaviourManager::GetNewBehaviour(A_Behaviour changeTo)
{
	switch (changeTo)
	{
	case A_BehaviourManager::SEEK:
		return new A_Seek();
	case A_BehaviourManager::HEAL:
		return new A_Heal();
	default:
		return new A_Wander();
	}
}


//IBehaviour
void HealerBehaviours::Reset()
{
	pathList.clear();
}

void HealerBehaviours::FollowPath(AmbulanceAgent* agent)
{
	if (pathList.empty())
		return;

	Node* waypoint = pathList.front();
	Vector2 direction = (waypoint->position - agent->transform.pos).Normalised();
	//move towards the first node in list
	agent->transform.pos += direction * agent->movementSpeed * DT;
	agent->transform.rotation = atan2f(direction.x, direction.y);

	//if we are standing on the node we asre heading towards
	if (GRAPH.FindNearestNode(agent->transform.pos) == waypoint)
	{
		//change standing node
		agent->standingNode = pathList.front();
		//remove node from list
		pathList.pop_front();
	}
}


//Wander
void A_Wander::Update(AmbulanceAgent* agent)
{
	agent->movementSpeed = 125.0f;
	float distance = 35.0f * 5;
	//if there is a dead agent near us
	if (agent->deadAgentsList.size() != 0 && (agent->FindClosestDeadAgent()->GetStandingNode()->position - agent->transform.pos).Magnitude() < 500)
	{
		//go to him
		agent->ChangeBehavior(A_BehaviourManager::SEEK);
		return;
	}
	// Make Path if none exist
	while (pathList.empty())
	{
		Node* startNode = GRAPH.FindNearestNode(agent->transform.pos);		// Get The Node The Agent Is In. This is where we start from
		Node* endNode = GRAPH.GetRandomNode();
		pathList = GRAPH.APath(startNode, endNode);
	}

	// Follow Path
	FollowPath(agent);
}

void A_Wander::Render()
{
	//GRAPH.RenderPath(pathList);
}


//Heal
A_Heal::A_Heal()
{
	healTime = 0;
}

void A_Heal::Update(AmbulanceAgent* agent)
{
	healTime += DT;
	Agent* closest = agent->FindClosestDeadAgent();

	//if there is no target
	if (agent->GetTarget() == nullptr)
	{
		//go back to wander
		agent->ChangeBehavior(A_BehaviourManager::WANDER);
		return;
	}

	//loop through agents list
	for (auto i = agent->agentsList.begin(); i != agent->agentsList.end(); ++i)
	{
		//skip if memory is empty
		if ((*i) == nullptr)
			continue;
		//if it is the dead agent we are healing
		else if ((*i) == closest)
		{
			//remove it from the agents list as he is dead
			i = agent->agentsList.erase(i);
		}
	}

	//after you have been healing for 2 seconds
	if (healTime > 2)
	{
		//revive that agent
		GAME.toRevive.push_back(closest);
		agent->deadAgentsList.remove(closest);
		agent->SetTarget(nullptr);
	}
}

//Seek
A_Seek::A_Seek()
{
	currentTime = 0;
}

void A_Seek::Update(AmbulanceAgent* agent)
{
	agent->movementSpeed = 85.0f;
	float distance = 35.0f * 5;
	Agent* closestDeadAgent = agent->FindClosestDeadAgent();

	//where you want to go - your pos
	if (agent->GetTarget() == nullptr)
		agent->SetTarget(closestDeadAgent);

	if (agent->GetStandingNode() == GRAPH.FindNearestNode(closestDeadAgent->transform.pos))
	{		
		agent->ChangeBehavior(A_BehaviourManager::HEAL);
		return;
	}

	// Find Path to target. Do IT SON!
	if (pathList.empty())
	{
		Node* startNode = GRAPH.FindNearestNode(agent->transform.pos);		// Get The Node The Agent Is In. This is where we start from
		Node* endNode = GRAPH.FindNearestNode(agent->GetTarget()->transform.pos);
		pathList = GRAPH.APath(startNode, endNode);

		// Still Empty? D: Guess we can't get there. Can't do this behaviour! ;_;
		if (pathList.empty())
		{
			agent->ChangeBehavior(A_BehaviourManager::WANDER);
			return;
		}
	}

	FollowPath(agent);
}

