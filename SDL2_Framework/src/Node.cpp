#include "Node.h"
#include "SDL.h"
#include "SDL_Framework.h"

Node::Node(std::string texName, Vector2 position)
{
	this->texName = texName;
	this->position = position;

	nScore = nullptr;
	gScore = INF;
	type = GROUND;
	terrainCost = 1;

	traversed = false;
}

void Node::ChangeTexture(std::string tex)
{
	texName = tex;
}

void Node::ChangeTileType(NodeType tile)
{
	type = tile;
	if (type == Node::WATER)
		terrainCost = 10;
	else if (type == Node::WALL)
		terrainCost = 100;
	else if (type == Node::GROUND)
		terrainCost = 1;
}

void Node::Render()
{
	Color texColor = TypeToColor(type);
	SCREEN.RenderTexture(TEXBANK.Load(texName), position.x, position.y, 0, 1, false, texColor);
}

void Node::RenderColor(Color color)
{
	SCREEN.RenderTexture(TEXBANK.Load(texName), position.x, position.y, 0, 1.0, false, color);
}

Color Node::TypeToColor(NodeType type)
{
	switch (type)
	{
		case Node::WATER:			return Color(0, 200, 200, 255);		break;
		case Node::WALL:			return Color::gray;					break;
		case Node::GROUND:			return Color::white;				break;
		default:					return Color::white;
	}
}