#pragma once
#include "Color.h"
#include <iostream>

class Node;

class Edge
{
public:
	
	Edge(Node* from, Node* to, Color color);

	/*
	Description: draws a line between the "from" and "to" nodes
	*/
	void Render();

	Node *from, *to;
	float cost;

private:
	Color color;
	bool render;
};