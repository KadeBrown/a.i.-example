#pragma once
#include "GameObj.h"
class eBullet : public GameObj
{
public:
	eBullet(Vector2 endPos, Vector2 startPos);
	~eBullet();

	/*
	Description: Updates position and checks for collision
	*/
	void Update();
	void Render();

private:
	float speed;
	Vector2 direction;
	Vector2	startPosition;
	Vector2 endPosition;
};

