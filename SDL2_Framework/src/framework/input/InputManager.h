#pragma once
#include "SDL_events.h"
#include "SDL_keyboard.h"
#include <map>

#define INPUTS (InputManager::_Instance())


class Vector2;

enum ButtonType : unsigned char
{
	MOUSE_LEFT = 0x0,
	MOUSE_RIGHT = 0x1,
	MOUSE_MIDDLE = 0x2,
};

class InputManager
{
public:
	enum InputState : unsigned char
	{
		UP = 0x1,	//resting up
		DOWN = 0x2,	//resting down
		PRESSED = 0x4,	//just pressed
		RELEASED = 0x8,	//just released
	};

	InputState GetKeyState(SDL_Scancode keyCode);
	bool GetKey(SDL_Scancode keyCode);
	bool GetKeyPressed(SDL_Scancode keyCode);
	bool GetKeyReleased(SDL_Scancode keyCode);

	InputState GetButtonState(ButtonType button);
	bool GetButton(ButtonType button);
	bool GetButtonPressed(ButtonType button);
	bool GetButtonReleased(ButtonType button);

	bool MouseScrollWheelUp();
	bool MouseScrollWheelDown();
	Vector2 GetMousePosition();
	void GetMousePosition(int* out_x, int* out_y);
	void GetMousePosition(float* out_x, float* out_y);

	static InputManager& _Instance();

private:
	friend class System;
	~InputManager(){}
	InputManager();
	InputManager(const InputManager&){};
	void operator = (const InputManager&){};

	void UpdateStates();
	static void UpdateMouseWheel(SDL_Event e);

	InputState keyStates[SDL_NUM_SCANCODES];

	int mx, my, mw;
	InputState buttonStates[8];
};

