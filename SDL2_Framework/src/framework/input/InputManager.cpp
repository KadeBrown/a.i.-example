#include "input/InputManager.h"
#include "system/System.h"
#include "graphics/Screen.h"
#include "SDL.h"
#include "Vector2.h"
#include <memory>

InputManager::InputManager() : mx(0), my(0), mw(0)
{
	memset(keyStates, UP, SDL_NUM_SCANCODES);
	memset(buttonStates, UP, 8);
	SYSTEM.RegisterEventCallback(SDL_MOUSEWHEEL, UpdateMouseWheel);
}

InputManager& InputManager::_Instance()
{
	static InputManager im;
	return im;
}

InputManager::InputState InputManager::GetKeyState(SDL_Scancode keyCode)
{
	return keyStates[keyCode];
}

bool InputManager::GetKey(SDL_Scancode keyCode)
{
	return (bool)(GetKeyState(keyCode) & (DOWN | PRESSED));
}

bool InputManager::GetKeyPressed(SDL_Scancode keyCode)
{
	return (bool)(GetKeyState(keyCode) == PRESSED);
}

bool InputManager::GetKeyReleased(SDL_Scancode keyCode)
{
	return (bool)(GetKeyState(keyCode) == RELEASED);
}

InputManager::InputState InputManager::GetButtonState(ButtonType button)
{
	return buttonStates[button];
}

bool InputManager::GetButton(ButtonType button)
{
	return (bool)(GetButtonState(button) & (DOWN | PRESSED));
}

bool InputManager::GetButtonPressed(ButtonType button)
{
	return (bool)(GetButtonState(button) == PRESSED);
}

bool InputManager::GetButtonReleased(ButtonType button)
{
	return (bool)(GetButtonState(button) == RELEASED);
}

bool InputManager::MouseScrollWheelUp()
{
	return mw == 1;
}

bool InputManager::MouseScrollWheelDown()
{
	return mw == -1;
}

Vector2 InputManager::GetMousePosition()
{
	return Vector2((float)mx - (SCREEN.GetWidth() * 0.5f), ((float)my - (SCREEN.GetHeight() * 0.5f)) * -1);
}

void InputManager::GetMousePosition(int* out_x, int* out_y)
{
	if (out_x != nullptr)
		*out_x = mx - (SCREEN.GetWidth() * 0.5f);
	if (out_y != nullptr)
		*out_y = my - (SCREEN.GetHeight() * 0.5f);
}

void InputManager::GetMousePosition(float* out_x, float* out_y)
{
	if (out_x != nullptr)
		*out_x = (float)mx - (SCREEN.GetWidth() * 0.5f);
	if (out_y != nullptr)
		*out_y = (float)my - (SCREEN.GetHeight() * 0.5f);
}

void InputManager::UpdateStates()
{
	//KEYS
	const Uint8* states_k = SDL_GetKeyboardState(NULL);

	for (int i = 0; i < SDL_NUM_SCANCODES; ++i)
	{
		if (states_k[i] == SDL_PRESSED)
			keyStates[i] = keyStates[i] & (DOWN | PRESSED) ? DOWN : PRESSED;
		else if (states_k[i] == SDL_RELEASED)
			keyStates[i] = keyStates[i] & (UP | RELEASED) ? UP : RELEASED;
	}


	//BUTTONS
	const Uint8 states_b = SDL_GetMouseState(&mx, &my);

	for (int i = 1; i < 9; ++i)
	{
		long l = (long)pow((double)i, 2);
		if (states_b & l)
			buttonStates[i - 1] = buttonStates[i - 1] & (DOWN | PRESSED) ? DOWN : PRESSED;
		else
			buttonStates[i - 1] = buttonStates[i - 1] & (UP | RELEASED) ? UP : RELEASED;
	}

	// Reset Mouse Wheel
	mw = 0;
}

void InputManager::UpdateMouseWheel(SDL_Event e)
{
	INPUTS.mw = e.wheel.y;
}