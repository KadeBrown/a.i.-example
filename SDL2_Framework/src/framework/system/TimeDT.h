#pragma once
#include "SDL_stdinc.h"

#define DT (Time::_Instance()).GetDT()

class Time
{
public:
	float GetDT() const;
	void UpdateDT();
	void SetTimeScale(float scale);

	static Time& _Instance();

private:
	Time();
	Time(const Time&) = delete;
	void operator = (const Time&) = delete;

	Uint32 baseTicks;
	float deltaTime;
	float timeScale;
};

