#include "System.h"
#include "TimeDT.h"
#include "InputManager.h"
#include "Screen.h"
#include "SDL.h"

System::System()
{
	SDL_Init(SDL_INIT_EVERYTHING);
	IMG_Init(IMG_INIT_PNG | IMG_INIT_JPG);
}

System::~System()
{
	SCREEN.Deinit();
	IMG_Quit();
	SDL_Quit();
}

System& System::_Instance()
{
	static System sy;
	return sy;
}

void System::Init(std::string windowName, Uint32 width, Uint32 height)
{
	SCREEN.Init(windowName, width, height);
}

void System::Update()
{
	SDL_PumpEvents();
	InputManager::_Instance().UpdateStates();
	Time::_Instance().UpdateDT();

	SDL_Event e;
	while (SDL_PollEvent(&e))
	{
		auto iter = callbacks.find(e.type);
		if (iter != callbacks.end())
		{
			std::vector<EventCallback>* calls = &(*iter).second;
			for (unsigned int i = 0; i < calls->size(); ++i)
				((*calls)[i])(e);
		}
	}
}

std::string System::GetResourcePath(const std::string& subDir)
{
#ifdef _WIN32
	const char PATH_SEP = '\\';
#else
	const char PATH_SEP = '/';
#endif

	static std::string baseRes;
	if (baseRes.empty())
	{
		char* basePath = SDL_GetBasePath();
		if (basePath){
			baseRes = basePath;
			SDL_free(basePath);
		}
		else {
			printf("Error getting resource path: %s", SDL_GetError());
			return "";
		}

		size_t pos = baseRes.rfind("bin");
		baseRes = baseRes.substr(0, pos) + "res" + PATH_SEP;
	}

	return subDir.empty() ? baseRes : baseRes + subDir + PATH_SEP;
}

void System::RegisterEventCallback(Uint32 eventID, EventCallback cb)
{
	if (callbacks.find(eventID) != callbacks.end())
		callbacks.insert(std::make_pair(eventID, std::vector<EventCallback>()));
	callbacks[eventID].push_back(cb);
}

void System::DeregisterEventCallback(Uint32 eventID, EventCallback cb)
{
	if (callbacks.find(eventID) == callbacks.end())
		return;

	std::vector<EventCallback>* calls = &(callbacks[eventID]);
	for (auto iter = (*calls).begin(); iter != (*calls).end(); iter++)
	{
		if ((*iter) == cb)
		{
			(*calls).erase(iter);
			return;
		}
	}
}