#pragma once
#include <string>
#include <map>
#include <vector>
#include "SDL_events.h"

#define SYSTEM (System::_Instance())

typedef void(*EventCallback)(SDL_Event e);

class System
{
public:
	void Init(std::string windowName, Uint32 width, Uint32 height);
	void Update();

	std::string GetResourcePath(const std::string& subDir = "");
	void RegisterEventCallback(Uint32 eventID, EventCallback cb);
	void DeregisterEventCallback(Uint32 eventID, EventCallback cb);

	static System& _Instance();

private:
	~System();
	System();
	System(const System&) = delete;
	void operator = (const System&) = delete;

	std::map<Uint32, std::vector<EventCallback>> callbacks;
};
