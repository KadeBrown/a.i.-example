#include "TimeDT.h"
#include "SDL.h"
#include "InputManager.h"

float Time::GetDT() const
{
	return deltaTime;
}

void Time::UpdateDT()
{
	Uint32 nowTicks = SDL_GetTicks();
	deltaTime = ((float)(nowTicks - baseTicks) / 1000) * timeScale;
	baseTicks = nowTicks;

	if (INPUTS.GetKeyPressed(SDL_SCANCODE_LCTRL))
	{
		timeScale = (timeScale > 0.2f) ? 0.1f : 1.0f;
	}
	else if (INPUTS.GetKey(SDL_SCANCODE_EQUALS))
	{
		timeScale += 0.1f;
	}
	else if (INPUTS.GetKey(SDL_SCANCODE_MINUS))
	{
		if (timeScale > 0.11f)
			timeScale -= 0.1f;
	}
	else if (INPUTS.GetKeyPressed(SDL_SCANCODE_BACKSPACE))
	{
		timeScale = 1.0f;
	}
}

void Time::SetTimeScale(float scale)
{
	timeScale = scale;
}

Time& Time::_Instance()
{
	static Time time;
	return time;
}

Time::Time()
{
	baseTicks = SDL_GetTicks();
	timeScale = 1.0f;
}