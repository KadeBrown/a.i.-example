#pragma once

//system
#include "System.h"
#include "TimeDT.h"
//input
#include "InputManager.h"
//graphics
#include "Screen.h"
#include "TextureBank.h"
#include "Texture.h"
#include "Color.h"
//maths
#include "Mat3.h"
#include "Mat4.h"
#include "Vec2.h"
#include "Vec3.h"
#include "Vec4.h"

#define PI 3.141592653f
#define PI2 6.283185307f