#include "Screen.h"
#include "SDL.h"
#include "SDL_opengl.h"
#include "Matrix4.h"

Screen& Screen::_Instance()
{
	static Screen s;
	return s;
}

void Screen::Deinit()
{
	SDL_GL_DeleteContext(context);
	SDL_DestroyWindow(window);
}

void Screen::Init(std::string windowName, Uint32 width, Uint32 height)
{
	this->width = width;
	this->height = height;
	this->aspect = (float)height / (float)width;
	this->pix = 2.f / height;
	
	window = SDL_CreateWindow(windowName.c_str(), SDL_WINDOWPOS_CENTERED, 
		SDL_WINDOWPOS_CENTERED, width, height, SDL_WINDOW_OPENGL);
	context = SDL_GL_CreateContext(window);
	SDL_GL_MakeCurrent(window, context);

	glEnable(GL_TEXTURE_2D);
	glDisable(GL_LIGHTING);
	//glEnable(GL_DEPTH_TEST);
	//glDepthFunc(GL_LESS);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glScalef(aspect, 1, 1);
}

void Screen::BeginRendering(float x, float y) const
{
	glClearColor(0.5f, 0.5f, 0.5f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glScalef(aspect, 1, 1);
	glTranslatef(-x * pix, -y * pix, 0);
}

void Screen::EndRendering() const
{
	SDL_GL_SwapWindow(window);
}

void Screen::RenderTexture(Texture tex, float x, float y, float rot, float scale, bool flip, Color color) const
{
	Matrix4 mat = (
		Matrix4::CreateScale(scale) *
		Matrix4::CreateRotationZ(rot)) *
		Matrix4::CreateTranslation(x * pix, y * pix, 0);

	glMatrixMode(GL_MODELVIEW);
	glLoadMatrixf(mat.GetPtr());
	RenderTexture(tex, flip, color);
}

void Screen::RenderTexture(Texture tex, const float* m4x4, bool flip, Color color) const
{
	glMatrixMode(GL_MODELVIEW);
	glLoadMatrixf(m4x4);
	RenderTexture(tex, flip, color);
}

void Screen::RenderLine(float x1, float y1, float x2, float y2, Color color) const
{
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glColor4ub(color.r, color.g, color.b, color.a);
	glBegin(GL_LINES);
	glVertex3f(x1 * pix, y1 * pix, 0);
	glVertex3f(x2 * pix, y2 * pix, 0);
	glEnd();
}

void Screen::RenderTexture(const Texture& tex, bool flip, Color color) const
{
	glBindTexture(GL_TEXTURE_2D, tex.GetTexID());

	float hW = tex.GetWidth() / 2.f;
	float hH = tex.GetHeight() / 2.f;

	glColor4ub(color.r, color.g, color.b, color.a);
	glBegin(GL_QUADS);

	flip ? glTexCoord2i(1, 1) : glTexCoord2i(0, 1);
	glVertex3f(-hW * pix, -hH * pix, 0.0f);
	flip ? glTexCoord2i(0, 1) : glTexCoord2i(1, 1);
	glVertex3f(hW * pix, -hH * pix, 0.0f);
	flip ? glTexCoord2i(0, 0) : glTexCoord2i(1, 0);
	glVertex3f(hW * pix, hH * pix, 0.0f);
	flip ? glTexCoord2i(1, 0) : glTexCoord2i(0, 0);
	glVertex3f(-hW * pix, hH * pix, 0.0f);
	glEnd();

	glBindTexture(GL_TEXTURE_2D, 0);
}