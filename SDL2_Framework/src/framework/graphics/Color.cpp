#include "Color.h"

Color Color::white = Color(0xFF, 0xFF, 0xFF, 0xFF);
Color Color::gray = Color(0x88, 0x88, 0x88, 0xFF);
Color Color::black = Color(0, 0, 0, 0xFF);
Color Color::red = Color(0xFF, 0, 0, 0xFF);
Color Color::green = Color(0, 0xFF, 0, 0xFF);
Color Color::blue = Color(0, 0, 0xFF, 0xFF);
Color Color::transparent = Color(0, 0, 0, 0);