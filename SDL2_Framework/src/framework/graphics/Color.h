#pragma once

#define byte unsigned char

class Color
{
public:
	Color() : r(0xFF), g(0xFF), b(0xFF), a(0xFF) { }
	Color(byte r, byte g, byte b, byte a) : r(r), g(g), b(b), a(a) { }

	byte r, g, b, a;

	static Color white;
	static Color gray;
	static Color black;
	static Color red;
	static Color green;
	static Color blue;
	static Color transparent;
};