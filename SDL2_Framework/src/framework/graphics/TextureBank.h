#pragma once
#include "SDL_render.h"
#include "Texture.h"
#include <map>
#include <string>
#include "SDL_opengl.h"

#define TEXBANK (TextureBank::_Instance())

class TextureBank
{
public:
	Texture Load(std::string path, GLenum format = GL_BGRA);
	void Clear();

	static TextureBank& _Instance();
private:
	~TextureBank();
	TextureBank(){}
	TextureBank(const TextureBank&) = delete;
	void operator = (const TextureBank&) = delete;

	void LoadTexture(std::string filename, Texture& texture, GLenum format);

	std::map<std::string, Texture> textures;
};

