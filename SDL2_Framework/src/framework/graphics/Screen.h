#pragma once
#include <string>
#include "SDL_stdinc.h"
#include "SDL_render.h"
#include "Texture.h"
#include "Color.h"

#define SCREEN (Screen::_Instance())

class Screen
{
public:
	inline int GetWidth() const { return width; }
	inline int GetHeight() const { return height; }
	inline float GetAspect() const { return aspect; }
	inline float GetPixUnit() const { return pix; }

	void BeginRendering(float x = 0, float y = 0) const;
	void EndRendering() const;
	void RenderTexture(Texture tex, float x, float y, float rot = 0, float scale = 1, bool flip = false, Color color = Color::white) const;
	void RenderTexture(Texture tex, const float* m4x4, bool flip = false, Color color = Color::white) const;
	void RenderLine(float x1, float y1, float x2, float y2, Color color = Color::white) const;

	static Screen& _Instance();

private:
	friend class System;
	~Screen(){}
	Screen(){}
	Screen(const Screen&) = delete;
	void operator = (const Screen&) = delete;

	void Init(std::string windowName, Uint32 width, Uint32 height);
	void Deinit();
	void RenderTexture(const Texture& tex, bool flip, Color color) const;

	SDL_Window* window;
	SDL_GLContext context;
	int width, height;
	float aspect, pix;
};

