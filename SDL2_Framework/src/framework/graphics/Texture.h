#pragma once
#include <string>
#include "SDL_opengl.h"

class Texture
{
public:
	std::string GetPath() const;
	unsigned int GetWidth() const;
	unsigned int GetHeight() const;
	GLuint GetTexID() const;

private:
	friend class TextureBank;
	Texture();

	std::string path;
	int width, height;
	GLuint texID;
};

