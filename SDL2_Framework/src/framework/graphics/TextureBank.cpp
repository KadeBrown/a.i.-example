#include "TextureBank.h"
#include "Color.h"
#include "System.h"
#include "SDL.h"

TextureBank& TextureBank::_Instance()
{
	static TextureBank tb;
	return tb;
}

TextureBank::~TextureBank()
{
	Clear();
}

Texture TextureBank::Load(std::string path, GLenum format)
{
	auto iter = textures.find(path);
	if (iter == textures.end())
	{
		Texture texture;
		texture.path = path;
		LoadTexture(SYSTEM.GetResourcePath() + path, texture, format);
		textures.insert(std::make_pair(path, texture));
		return texture;
	}
	return (*iter).second;
}

void TextureBank::Clear()
{
	for (auto iter = textures.begin(); iter != textures.end(); iter++)
	{
		GLuint id = (*iter).second.GetTexID();
		glDeleteTextures(1, &id);
	}
	textures.clear();
}

void TextureBank::LoadTexture(std::string filename, Texture& texture, GLenum format)
{
	SDL_Surface* surface = IMG_Load(filename.c_str());
	if (!surface)
	{
		printf("image failed to load %s", filename.c_str());
		return;
	}

	int BPP = surface->format->BytesPerPixel;
	GLenum textureFormat;

	if (surface->format->BytesPerPixel == 4)
		textureFormat = (SDL_BYTEORDER == SDL_BIG_ENDIAN) ? GL_BGRA : GL_RGBA;
	else if (surface->format->BytesPerPixel == 3)
		textureFormat = (SDL_BYTEORDER == SDL_BIG_ENDIAN) ? GL_BGR : GL_RGB;

	GLuint id;
	glGenTextures(1, &id);
	texture.texID = id;
	texture.width = surface->w;
	texture.height = surface->h;

	glBindTexture(GL_TEXTURE_2D, texture.texID);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexImage2D(GL_TEXTURE_2D, 0, textureFormat, texture.width, texture.height, 0,
		textureFormat, GL_UNSIGNED_BYTE, surface->pixels);

	glBindTexture(GL_TEXTURE_2D, 0);
}