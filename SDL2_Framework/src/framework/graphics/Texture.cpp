#include "Texture.h"

Texture::Texture() : path("none"), width(0), height(0), texID(0)
{

}

std::string Texture::GetPath() const
{
	return path;
}

unsigned int Texture::GetWidth() const
{
	return width;
}

unsigned int Texture::GetHeight() const
{
	return height;
}

GLuint Texture::GetTexID() const
{
	return texID;
}