#pragma once
#include "Edge.h"
#include "Vector2.h"
#include <vector>
#include <limits>

#define INF std::numeric_limits<float>::max()

class Node
{
public:

	Node(std::string texName, Vector2 position);

	enum NodeType
	{
		START,
		END,
		WALL,
		WATER,
		GROUND
	};

	void Render();

	/*
	Description: renders the texture in a different colour
	Param: the colour you wish to render the texture as
	*/
	void RenderColor(Color color);
	void ChangeTexture(std::string tex);
	void ChangeTileType(NodeType tile);

	/*
	Description: calculates what colour the nose will need to be which is determined by its tile type
	Param: the type that the node is
	Returns: the color the node should be
	*/
	static Color TypeToColor(NodeType type);

	Vector2 position;
	std::vector<Edge*> edges;
	//N score , its previous node
	Node* nScore;
	//G Score, its costAtTime
	float gScore;
	float fScore, hScore;
	NodeType type;
	bool traversed;
	int terrainCost;
private:

	std::string texName;
};

