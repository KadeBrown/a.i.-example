#include "Edge.h"
#include "framework\SDL_Framework.h"
#include "Node.h"

Edge::Edge(Node* from, Node* to, Color color)
{
	cost = (from->position - to->position).Magnitude();
	this->from = from;
	this->to = to;
	this->color = color;
	render = false;
}

void Edge::Render()
{
	//render if space has been pressed
	if (INPUTS.GetKeyPressed(SDL_SCANCODE_SPACE) && render == true)
		render = false;
	 else if (INPUTS.GetKeyPressed(SDL_SCANCODE_SPACE) && render == false)
		render = true;
	if (render == true)
		SCREEN.RenderLine(from->position.x, from->position.y, to->position.x, to->position.y, color);
}