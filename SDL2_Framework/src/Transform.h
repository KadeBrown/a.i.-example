#pragma once
#include <vector>
#include "Vector2.h"
#include "Matrix4.h"

class Transform
{
public:
	Transform();
	~Transform();

	static Transform& GetRoot();

	void AddChild(Transform* child);
	void RemoveChild(Transform* child);

	inline Matrix4 GetGlobalTransform() { return globalTransform; }
	inline Matrix4 GetLocalTransform() { return localTransform; }

	Vector3 GetGlobalUp();
	Vector3 GetGlobalRight();
	Vector2 GetGlobalPosition();
	float GetGlobalRotation();
	float GetGlobalScale();

	void Update();

	Vector2 pos;
	float rotation;
	float scale;
	Transform* parent;

private:
	Transform(bool root);
	Matrix4 localTransform;
	Matrix4 globalTransform;

	std::vector<Transform*> children;
};

