#include "Agent.h"
#include "Game.h"
#include "Graph.h"
#include "AgentGun.h"

Agent::Agent(const char* textureToLoad) : GameObj(textureToLoad)
{
	objType = AGENT;
	behaviourManager = new BehaviourManager(this);
	behaviourManager->ChangeBehaviour(BehaviourManager::WANDER);

	SetSpawn();

	hasGun = false;
	target = nullptr;

	movementSpeed = 100.0f;
	transform.scale = 0.1f;
	shootTimer = 0;

	standingNode = GRAPH.FindNearestNode(transform.pos);
	agentsList.push_back(this);
}

Agent::~Agent()
{
	agentsList.remove(this);
	delete behaviourManager;

	new Agent("BlueAgent.png");
	new Agent("BlueAgent.png");
}

void Agent::SetSpawn()
{
	Node* spawnNode = GRAPH.GetRandomNode();
	transform.pos = spawnNode->position;
}

void Agent::Update()
{
	Vector2 currentPos = transform.pos;
	behaviourManager->Update();

	//if we are dead
	for (auto i = deadAgentsList.begin(); i != deadAgentsList.end(); ++i)
	{
		//dont move
		if ((*i) == this)
			transform.pos = currentPos;
	}

	//are we standing on gun?
	for (auto gun = gunsList.begin(); gun != gunsList.end(); ++gun)
	{
		if ((*gun)->GetStandingNode() == this->standingNode)
		{
			//pickup gun
			OnCollision( *gun );
			(*gun)->OnCollision(this);
		}
	}
}

void Agent::Render()
{
	behaviourManager->Render();
	switch (behaviourManager->GetCurrentBehaviour())
	{
		case BehaviourManager::FLEE:
			SCREEN.RenderTexture(TEXBANK.Load("GreenAgent.png"), transform.pos.x, transform.pos.y, transform.rotation, transform.scale, false, Color::white);
			break;
		case BehaviourManager::WANDER:
			SCREEN.RenderTexture(TEXBANK.Load("BlueAgent.png"), transform.pos.x, transform.pos.y, transform.rotation, transform.scale, false, Color::white);
			break;
		case BehaviourManager::SEEK:
			SCREEN.RenderTexture(TEXBANK.Load("RedAgent.png"), transform.pos.x, transform.pos.y, transform.rotation, transform.scale, false, Color::white);
			break;
		default:
			GameObj::Render();
	}

	if (hasGun == true && behaviourManager->GetCurrentBehaviour() != BehaviourManager::SEEK)
		SCREEN.RenderTexture(TEXBANK.Load("BlueAgent.png"), transform.pos.x, transform.pos.y, transform.rotation, transform.scale, false, Color::black);
}

void Agent::ReturnToPreviousBehavior()
{
	behaviourManager->ReturnToPreviousBehaviour();
}

void Agent::ChangeBehavior(BehaviourManager::Behaviour newBehavior)
{
	behaviourManager->ChangeBehaviour(newBehavior);
}

void Agent::SetTarget(GameObj* newTarget)
{
	target = newTarget;
}

void Agent::OnCollision(GameObj* other)
{
	// have we reached our target
	if (other == target)
	{
		target = nullptr;
	}

	if (other->objType == GameObj::GUN)
	{
		hasGun = true;
	}
}

GameObj* Agent::GetTarget()
{
	return target;
}

Agent* Agent::FindClosestAgent()
{
	Agent* closestAgent = nullptr;
	float distance = INF;
	// loop through all agents
	for (auto i = agentsList.begin(); i != agentsList.end(); ++i)
	{
		// if there is no agent, skip to next one in list
		if ((*i) == nullptr)
			continue;
		// are they closer than the previous agent
		else if ((*i) != this && distance > (transform.pos - (*i)->transform.pos).Magnitude())
		{
			distance = (transform.pos - (*i)->transform.pos).Magnitude();
			closestAgent = (*i);
		}
		
	}
	return closestAgent;
}

Agent* Agent::FindClosestThreat()
{
	Agent* closestAgent = nullptr;
	float distance = INF;
	for (auto i = agentsList.begin(); i != agentsList.end(); ++i)
	{
		if ((*i) == nullptr)
			continue;
		else if ((*i) != this && (*i)->hasGun == true && distance > (transform.pos - (*i)->transform.pos).Magnitude())
		{
			distance = (transform.pos - (*i)->transform.pos).Magnitude();
			closestAgent = (*i);
		}
		else
		{
			closestAgent = closestAgent;
		}

	}
	return closestAgent;
}

Agent* Agent::FindClosestNonThreat()
{
	Agent* closestAgent = nullptr;
	float distance = INF;

	for (auto i = agentsList.begin(); i != agentsList.end(); ++i)
	{
		if ((*i) == nullptr)
			continue;
		else if ((*i) != this && (*i)->hasGun == false && distance > (transform.pos - (*i)->transform.pos).Magnitude() && (*i) != nullptr)
		{
			distance = (transform.pos - (*i)->transform.pos).Magnitude();
			closestAgent = (*i);
		}
		else
		{
			closestAgent = closestAgent;
		}

	}
	return closestAgent;
}

AgentGun* Agent::FindClosestGun()
{
	AgentGun* closest = nullptr;
	float distance = INF;
	//loop through all guns
	for (auto i = gunsList.begin(); i != gunsList.end(); ++i)
	{
		//is it closer than the previous
		if (distance > (transform.pos - (*i)->transform.pos).Magnitude())
		{
			//new closest gun
			distance = (transform.pos - (*i)->transform.pos).Magnitude();
			closest = (*i);
		}

	}
	return closest;
}

