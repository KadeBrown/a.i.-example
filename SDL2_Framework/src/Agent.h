#pragma once
#include "GameObj.h"
#include "IBehavior.h"

class AgentGun;

class Agent : public GameObj
{
public:
	Agent(const char* textureToLoad);
	~Agent();

	/*
	Description: Updates position and checks if its dead or standing on a gun
	*/
	virtual void Update();

	/*
	Description: renders the agent according to behaviour state
	*/
	void Render();

	/*
	Description: sets current behaviour to its previous behaviour
	*/
	void ReturnToPreviousBehavior();

	/*
	Description: Changes current behaviour to a given behaviour
	Param: a behaviour to change to
	*/
	void ChangeBehavior(BehaviourManager::Behaviour newBehavior);

	/*
	Description: Sets the agents target to a given object
	Param: a game object for the agent to target
	*/
	void SetTarget(GameObj* target);

	/*
	Description: picks up weapon if it collided with one
	Param: the object the agent has collided with
	*/
	void OnCollision(GameObj* other);

	/*
	Description: sets the agents spawn point to a random position
	*/
	void SetSpawn();


	/*
	Description: finds closest agent to itself
	Returns: closest agent
	*/
	Agent* FindClosestAgent();

	/*
	Description: finds closest agent with a gun
	Returns: closest agent with a gun
	*/
	Agent* FindClosestThreat();

	/*
	Description: finds closest agent without a gun
	Returns: closest agent without a gun
	*/
	Agent* FindClosestNonThreat();

	/*
	Description: finds closest gun
	Returns: closest gun
	*/
	AgentGun* FindClosestGun();

	/*
	Returns: returns agents target
	*/
	GameObj* GetTarget();

	float movementSpeed;
	bool hasGun;
	float shootTimer;

protected:
	friend class IBehaviour;
	friend class HealerBehaviours;

private:
	BehaviourManager* behaviourManager;
	GameObj* target;

};

